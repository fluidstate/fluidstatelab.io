import { RollupOptions } from "rollup";
import { babel } from "@rollup/plugin-babel";
import { nodeResolve } from "@rollup/plugin-node-resolve";
import dynamicImportVars from "@rollup/plugin-dynamic-import-vars";
import commonjs from "@rollup/plugin-commonjs";
import { readdirSync } from "fs";
import { join } from "path";
import replace from "@rollup/plugin-replace";
import terser from "@rollup/plugin-terser";

// @ts-ignore
import { string } from "rollup-plugin-string";
// @ts-ignore
import static_files from "rollup-plugin-static-files";

const extensions = [".js", ".jsx", ".ts", ".tsx"];

const pageDir = "./src/pages";

const options: RollupOptions = {
	input: [
		...readdirSync(join(__dirname, pageDir)).map(
			(pageFile) => `${pageDir}/${pageFile}`
		),
		"src/playground/playground-fluidstate.ts",
		"src/playground/playground-preact.tsx",
		"src/playground/playground-react.tsx",
	],
	output: {
		dir: "public",
		entryFileNames: "[name].js",
		// sourcemap: true,
	},
	plugins: [
		babel({
			babelHelpers: "bundled",
			extensions,
		}),
		replace({
			preventAssignment: true,
			values: {
				"process.env.NODE_ENV": JSON.stringify("production"),
			},
		}),
		string({
			include: "**/*.md",
		}),
		nodeResolve({ extensions, browser: true }),
		dynamicImportVars(),
		commonjs(),
		static_files({
			include: ["./static"],
		}),
		// terser({
		// 	sourceMap: true,
		// }),
	],
};

export default options;
