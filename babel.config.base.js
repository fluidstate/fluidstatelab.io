module.exports.createBaseBabelConfig = () => ({
	// inputSourceMap: true,
	// sourceMaps: true,
	presets: [
		["@babel/preset-env", { targets: "defaults" }],
		["@babel/preset-typescript"],
		[
			"@babel/preset-react",
			{
				pragma: "preact.h",
				pragmaFrag: "preact.Fragment",
			},
		],
	],
	plugins: [
		[
			"module-resolver",
			{
				alias: {
					react: "preact/compat",
					"react-dom/test-utils": "preact/test-utils",
					"react-dom": "preact/compat",
					"react/jsx-runtime": "preact/jsx-runtime",
				},
			},
		],
	],
});
