import { RollupOptions } from "rollup";
import { babel } from "@rollup/plugin-babel";
import { nodeResolve } from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import replace from "@rollup/plugin-replace";
import dynamicImportVars from "@rollup/plugin-dynamic-import-vars";

// @ts-ignore
import { string } from "rollup-plugin-string";
// @ts-ignore
import static_files from "rollup-plugin-static-files";

const extensions = [".js", ".jsx", ".ts", ".tsx"];

const options: RollupOptions = {
	input: [
		"src/generate/generate.tsx",
		"src/generate/generate-pages.ts",
		"src/generate/generate-doc-pages.ts",
	],
	output: {
		dir: "dist",
		entryFileNames: "[name].mjs",
		chunkFileNames: "[name].[hash].mjs",
	},
	plugins: [
		babel({
			babelHelpers: "bundled",
			extensions,
		}),
		replace({
			preventAssignment: true,
			values: {
				"process.env.NODE_ENV": JSON.stringify("production"),
			},
		}),
		string({
			include: "**/*.md",
		}),
		nodeResolve({ extensions }),
		dynamicImportVars(),
		commonjs(),
		static_files({
			include: ["./static"],
		}),
	],
	onwarn: (warning, defaultHandler) => {
		if (warning.code === "CIRCULAR_DEPENDENCY") {
			return;
		}
		defaultHandler(warning);
	},
};

export default options;
