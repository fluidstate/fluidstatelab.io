import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/api-primitives-actions-effects.md";

export const page = declarePage({
	name: "api-primitives-actions-effects",
	title: "Fluid State - Primitives: Actions and Effects",
	url: "api-primitives-actions-effects",
	Root: () => <Markdown>{content}</Markdown>,
});
