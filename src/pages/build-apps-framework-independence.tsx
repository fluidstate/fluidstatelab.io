import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/build-apps-framework-independence.md";

export const page = declarePage({
	name: "build-apps-framework-independence",
	title: "Fluid State - Framework independence",
	url: "build-apps-framework-independence",
	Root: () => <Markdown>{content}</Markdown>,
});
