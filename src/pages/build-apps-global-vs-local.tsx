import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/build-apps-global-vs-local.md";

export const page = declarePage({
	name: "build-apps-global-vs-local",
	title: "Fluid State - Global vs. local state",
	url: "build-apps-global-vs-local",
	Root: () => <Markdown>{content}</Markdown>,
});
