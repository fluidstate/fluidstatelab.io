import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/build-apps-separate-view-data.md";

export const page = declarePage({
	name: "build-apps-separate-view-data",
	title: "Fluid State - Separate view and data",
	url: "build-apps-separate-view-data",
	Root: () => <Markdown>{content}</Markdown>,
});
