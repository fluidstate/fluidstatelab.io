import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/example-data-filtering.md";

export const page = declarePage({
	name: "example-data-filtering",
	title: "Fluid State - Example: Data filtering",
	url: "example-data-filtering",
	Root: () => <Markdown>{content}</Markdown>,
});
