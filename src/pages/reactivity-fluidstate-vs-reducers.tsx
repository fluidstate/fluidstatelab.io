import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/reactivity-fluidstate-vs-reducers.md";

export const page = declarePage({
	name: "reactivity-fluidstate-vs-reducers",
	title: "Fluid State - Fluid State vs. Reducers",
	url: "reactivity-fluidstate-vs-reducers",
	Root: () => <Markdown>{content}</Markdown>,
});
