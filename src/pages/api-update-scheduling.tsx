import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/api-update-scheduling.md";

export const page = declarePage({
	name: "api-update-scheduling",
	title: "Fluid State - Update scheduling",
	url: "api-update-scheduling",
	Root: () => <Markdown>{content}</Markdown>,
});
