import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/example-form-validation.md";

export const page = declarePage({
	name: "example-form-validation",
	title: "Fluid State - Example: Form validation",
	url: "example-form-validation",
	Root: () => <Markdown>{content}</Markdown>,
});
