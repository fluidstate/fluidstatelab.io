import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/reactivity-fluidstate-vs-solid.md";

export const page = declarePage({
	name: "reactivity-fluidstate-vs-solid",
	title: "Fluid State - Fluid State vs. Solid Signals",
	url: "reactivity-fluidstate-vs-solid",
	Root: () => <Markdown>{content}</Markdown>,
});
