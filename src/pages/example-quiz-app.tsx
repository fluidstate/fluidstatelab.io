import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/example-quiz-app.md";

export const page = declarePage({
	name: "example-quiz-app",
	title: "Fluid State - Example: Quiz app",
	url: "example-quiz-app",
	Root: () => <Markdown>{content}</Markdown>,
});
