import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/build-apps-presentational-only.md";

export const page = declarePage({
	name: "build-apps-presentational-only",
	title: "Fluid State - Write only presentational components",
	url: "build-apps-presentational-only",
	Root: () => <Markdown>{content}</Markdown>,
});
