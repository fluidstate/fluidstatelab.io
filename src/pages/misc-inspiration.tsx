import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/misc-inspiration.md";

export const page = declarePage({
	name: "misc-inspiration",
	title: "Fluid State - Inspiration",
	url: "misc-inspiration",
	Root: () => <Markdown>{content}</Markdown>,
});
