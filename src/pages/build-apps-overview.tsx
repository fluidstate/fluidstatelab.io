import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/build-apps-overview.md";

export const page = declarePage({
	name: "build-apps-overview",
	title: "Fluid State - How to build reactive apps",
	url: "build-apps-overview",
	Root: () => <Markdown>{content}</Markdown>,
});
