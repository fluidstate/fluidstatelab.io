import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/example-puzzle-game.md";

export const page = declarePage({
	name: "example-puzzle-game",
	title: "Fluid State - Example: Puzzle game",
	url: "example-puzzle-game",
	Root: () => <Markdown>{content}</Markdown>,
});
