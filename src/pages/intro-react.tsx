import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/intro-react.md";

export const page = declarePage({
	name: "intro-react",
	title: "Fluid State - Fluid State with React",
	url: "intro-react",
	Root: () => <Markdown>{content}</Markdown>,
});
