import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/api-reactive-inert-utilities.md";

export const page = declarePage({
	name: "api-reactive-inert-utilities",
	title: "Fluid State - Reactive and inert utilities",
	url: "api-reactive-inert-utilities",
	Root: () => <Markdown>{content}</Markdown>,
});
