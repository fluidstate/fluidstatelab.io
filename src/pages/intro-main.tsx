import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/intro-main.md";

export const page = declarePage({
	name: "intro-main",
	title: "Fluid State - Simple, Powerful, Reactive State Management",
	url: "index.html",
	Root: () => <Markdown>{content}</Markdown>,
});
