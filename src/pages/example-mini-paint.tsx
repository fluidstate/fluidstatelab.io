import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/example-mini-paint.md";

export const page = declarePage({
	name: "example-mini-paint",
	title: "Fluid State - Example: Mini paint",
	url: "example-mini-paint",
	Root: () => <Markdown>{content}</Markdown>,
});
