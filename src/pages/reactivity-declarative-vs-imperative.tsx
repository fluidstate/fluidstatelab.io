import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/reactivity-declarative-vs-imperative.md";

export const page = declarePage({
	name: "reactivity-declarative-vs-imperative",
	title: "Fluid State - Declarative vs. imperative code",
	url: "reactivity-declarative-vs-imperative",
	Root: () => <Markdown>{content}</Markdown>,
});
