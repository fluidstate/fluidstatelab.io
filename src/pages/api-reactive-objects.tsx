import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/api-reactive-objects.md";

export const page = declarePage({
	name: "api-reactive-objects",
	title: "Fluid State - Reactive objects",
	url: "api-reactive-objects",
	Root: () => <Markdown>{content}</Markdown>,
});
