import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/example-todo.md";

export const page = declarePage({
	name: "example-todo",
	title: "Fluid State - Example: To-do app",
	url: "example-todo",
	Root: () => <Markdown>{content}</Markdown>,
});
