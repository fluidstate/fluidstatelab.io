import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/build-apps-ssr-csr.md";

export const page = declarePage({
	name: "build-apps-ssr-csr",
	title: "Fluid State - Server and client side rendering",
	url: "build-apps-ssr-csr",
	Root: () => <Markdown>{content}</Markdown>,
});
