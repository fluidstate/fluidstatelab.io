import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/api-primitives-observed-computed.md";

export const page = declarePage({
	name: "api-primitives-observed-computed",
	title: "Fluid State - Primitives: Observed and Computed",
	url: "api-primitives-observed-computed",
	Root: () => <Markdown>{content}</Markdown>,
});
