import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/api-batching.md";

export const page = declarePage({
	name: "api-batching",
	title: "Fluid State - Batching",
	url: "api-batching",
	Root: () => <Markdown>{content}</Markdown>,
});
