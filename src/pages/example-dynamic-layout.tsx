import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/example-dynamic-layout.md";

export const page = declarePage({
	name: "example-dynamic-layout",
	title: "Fluid State - Example: Dynamic layout",
	url: "example-dynamic-layout",
	Root: () => <Markdown>{content}</Markdown>,
});
