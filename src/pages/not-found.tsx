import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/not-found.md";

export const page = declarePage({
	name: "not-found",
	title: "Fluid State - Page not found",
	url: "not-found",
	Root: () => <Markdown>{content}</Markdown>,
});
