import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/api-reactive-effects.md";

export const page = declarePage({
	name: "api-reactive-effects",
	title: "Fluid State - Reactive effects",
	url: "api-reactive-effects",
	Root: () => <Markdown>{content}</Markdown>,
});
