import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/reactivity-fluidstate-goal.md";

export const page = declarePage({
	name: "reactivity-fluidstate-goal",
	title: "Fluid State - Goal of Fluid State",
	url: "reactivity-fluidstate-goal",
	Root: () => <Markdown>{content}</Markdown>,
});
