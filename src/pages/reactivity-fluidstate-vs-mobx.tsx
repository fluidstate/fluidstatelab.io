import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/reactivity-fluidstate-vs-mobx.md";

export const page = declarePage({
	name: "reactivity-fluidstate-vs-mobx",
	title: "Fluid State - Fluid State vs. MobX",
	url: "reactivity-fluidstate-vs-mobx",
	Root: () => <Markdown>{content}</Markdown>,
});
