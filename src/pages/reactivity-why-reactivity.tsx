import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/reactivity-why-reactivity.md";

export const page = declarePage({
	name: "reactivity-why-reactivity",
	title: "Fluid State - Why reactivity?",
	url: "reactivity-why-reactivity",
	Root: () => <Markdown>{content}</Markdown>,
});
