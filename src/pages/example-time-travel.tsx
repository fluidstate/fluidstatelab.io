import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/example-time-travel.md";

export const page = declarePage({
	name: "example-time-travel",
	title: "Fluid State - Example: Time travel",
	url: "example-time-travel",
	Root: () => <Markdown>{content}</Markdown>,
});
