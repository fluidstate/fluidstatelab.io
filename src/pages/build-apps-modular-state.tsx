import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/build-apps-modular-state.md";

export const page = declarePage({
	name: "build-apps-modular-state",
	title: "Fluid State - Modular state",
	url: "build-apps-modular-state",
	Root: () => <Markdown>{content}</Markdown>,
});
