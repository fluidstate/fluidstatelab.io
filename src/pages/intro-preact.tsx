import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/intro-preact.md";

export const page = declarePage({
	name: "intro-preact",
	title: "Fluid State - Fluid State with Preact",
	url: "intro-preact",
	Root: () => <Markdown>{content}</Markdown>,
});
