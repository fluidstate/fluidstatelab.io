import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/misc-license.md";

export const page = declarePage({
	name: "misc-license",
	title: "Fluid State - License",
	url: "misc-license",
	Root: () => <Markdown>{content}</Markdown>,
});
