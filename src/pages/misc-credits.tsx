import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/misc-credits.md";

export const page = declarePage({
	name: "misc-credits",
	title: "Fluid State - Credits",
	url: "misc-credits",
	Root: () => <Markdown>{content}</Markdown>,
});
