import { declarePage } from "../common";
import { Markdown } from "../components/markdown";
import content from "../docs/reactivity-event-systems-vs-reactivity.md";

export const page = declarePage({
	name: "reactivity-event-systems-vs-reactivity",
	title: "Fluid State - Event systems vs. reactivity",
	url: "reactivity-event-systems-vs-reactivity",
	Root: () => <Markdown>{content}</Markdown>,
});
