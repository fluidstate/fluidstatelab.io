import { promises } from "fs";
import { join, dirname, relative, basename } from "path";
import { render } from "preact-render-to-string";
import createEmotionServer from "@emotion/server/create-instance";
import { cache } from "@emotion/css";
import { argv, cwd } from "node:process";
import { setClientSide } from "../common/client-side";
import { Page } from "../common/page";
import { App } from "../components/app";
import {
	defineGlobalPreact,
	extensionRegex,
	getPageFileNameWithoutExtension,
} from "./generate-common";

const { extractCritical } = createEmotionServer(cache);

const main = async (pageFile: string) => {
	setClientSide(false);
	defineGlobalPreact();
	const fileName = basename(pageFile);
	const template = await promises.readFile(
		join(cwd(), "./src/generate/template.html"),
		"utf-8"
	);
	const { page } = (await import(
		`../pages/${getPageFileNameWithoutExtension(pageFile)}.tsx`
	)) as {
		page: Page;
	};
	const { title, url } = page;
	const htmlUrl = /\.(html|htm)$/.test(url)
		? join(cwd(), "./public", url)
		: join(cwd(), "./public", url, "index.html");
	const htmlDirUrl = dirname(htmlUrl);
	const pageScriptUrl = join(cwd(), "./public", fileName).replace(
		extensionRegex,
		".js"
	);
	const { html, css, ids } = extractCritical(
		render(<App url={{ pathname: url, search: "", hash: "" }} page={page} />)
	);
	const content = template
		.replace("<!-- title -->", title)
		.replace("$cssKey", cache.key)
		.replace("$cssIds", ids.join(" "))
		.replace("/* cssContent */", css)
		.replace("<!-- body -->", html)
		.replace(
			"<!-- script -->",
			`<script type="module" src="${relative(
				htmlDirUrl,
				pageScriptUrl
			)}"></script>`
		);
	await promises.mkdir(htmlDirUrl, { recursive: true });
	await promises.writeFile(htmlUrl, content);
};

main(argv[2]);
