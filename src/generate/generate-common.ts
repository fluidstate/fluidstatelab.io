import { h, Fragment } from "preact";
import { basename } from "path";

export const defineGlobalPreact = () => {
	const value = { h, Fragment };
	Object.freeze(value);
	Object.defineProperty(globalThis, "preact", {
		configurable: false,
		enumerable: false,
		writable: false,
		value,
	});
};

export const extensionRegex = /\.(ts|tsx|js|jsx)$/;

export const getPageFileNameWithoutExtension = (pageFilePath: string) => {
	return basename(pageFilePath).replace(extensionRegex, "");
};
