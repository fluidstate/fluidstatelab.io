import { promises } from "fs";
import { basename, join } from "path";
import { cwd } from "node:process";

const main = async () => {
	const docFiles = await promises.readdir(join(cwd(), "./src/docs"));
	const asyncDocTitles: Array<Promise<string>> = docFiles
		.map((docFile) =>
			promises.readFile(join(cwd(), `./src/docs/${docFile}`), {
				encoding: "utf-8",
			})
		)
		.map((promise) =>
			promise.then((content) => content.match(/^#\s*(.*?)\n/)?.[1] ?? "unknown")
		);
	for (const [i, asyncDocTitle] of asyncDocTitles.entries()) {
		const docFile = docFiles[i];
		const name = docFile.replace(/\.md$/, "");
		const title = await asyncDocTitle;
		const content = `
			import { declarePage } from "../common";
			import { Markdown } from "../components/markdown";
			import content from "../docs/${docFile}";
			
			export const page = declarePage({
				name: ${JSON.stringify(name)},
				title: ${JSON.stringify(
					name === "intro-main"
						? `Fluid State - Simple, Powerful, Reactive State Management`
						: `Fluid State - ${title}`
				)},
				url: ${JSON.stringify(name === "intro-main" ? "index.html" : name)},
				Root: () => <Markdown>{content}</Markdown>,
			});
		`;
		await promises.writeFile(join(cwd(), `./src/pages/${name}.tsx`), content);
	}
};

main();
