import { promises } from "fs";
import { join } from "path";
import { Page } from "../common/page";
import { setClientSide } from "../common/client-side";
import {
	defineGlobalPreact,
	getPageFileNameWithoutExtension,
} from "./generate-common";
import { cwd } from "node:process";

const extensionRegex = /\.(ts|tsx|js|jsx)$/;

const main = async () => {
	setClientSide(false);
	defineGlobalPreact();
	const pageFiles = await promises.readdir(join(cwd(), "./src/pages"));
	const generatedPages: Record<string, { title: string; url: string }> = {};
	const asyncPages: Array<Promise<{ page: Page }>> = pageFiles.map(
		(pageFile) =>
			import(`../pages/${getPageFileNameWithoutExtension(pageFile)}.tsx`)
	);
	for (const [i, asyncPage] of asyncPages.entries()) {
		const pageFile = pageFiles[i];
		const { page } = await asyncPage;
		const { title, url } = page;
		generatedPages[pageFile.replace(extensionRegex, "")] = { title, url };
	}
	await promises.writeFile(
		join(cwd(), "./src/common/pages-generated.ts"),
		`export type PageName = keyof typeof Pages;` +
			`\n\n` +
			`// Note: this file is auto generated. Just run "yarn build"\n` +
			`export const Pages = ${JSON.stringify(generatedPages)};`
	);
};

main();
