import { ComponentChildren, FunctionComponent, RefCallback } from "preact";
import { useStore } from "../common/store-context";
import { PageLink } from "./page-link";
import { getInert } from "fluidstate";
import { css, keyframes } from "../common/css";
import { MainTheme } from "../common/theme";
import { Burger } from "./burger";
import {
	MOBILE,
	NON_MOBILE,
	WIDESCREEN,
	WIDESCREEN_BREAKPOINT,
} from "../common/media-const";
import { PageName } from "../common/pages-generated";
import { Link } from "./link";
import { useLayoutEffect, useRef } from "preact/hooks";

const HEADER_SIZE = 60;

const NAVIGATION_SIZE = 250;

const MOBILE_NAVIGATION_SIZE = 350;

const WIDESCREEN_NAVIGATION_SIZE_CALC = `calc(50% - ${Math.round(
	WIDESCREEN_BREAKPOINT / 2 - NAVIGATION_SIZE
)}px)`;

const headerClass = css`
	padding: 10px 10px 0px 10px;
	background-color: #13162e;
	background-image: url(/fluidstate-main-bg.png);
	height: ${HEADER_SIZE}px;
	border-bottom: 3px solid ${MainTheme.mainBorderColor};
	white-space: nowrap;
	overflow-x: auto;
`;

const navigationClass = css`
	width: ${NAVIGATION_SIZE}px;
	text-align: right;
`;

const mainClass = css`
	overflow: auto;
`;

const mainContentClass = css`
	max-width: ${WIDESCREEN_BREAKPOINT - NAVIGATION_SIZE}px;
`;

const layoutClass = css`
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;

	.${headerClass} {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		right: 0;
		overflow: hidden;
		z-index: 100;
	}

	.${navigationClass} {
		position: absolute;
		top: ${HEADER_SIZE}px;
		left: 0;
		width: ${NAVIGATION_SIZE}px;
		bottom: 0;
		overflow: auto;
		z-index: 50;

		@media ${WIDESCREEN} {
			width: ${WIDESCREEN_NAVIGATION_SIZE_CALC};
		}
	}

	.${mainClass} {
		position: absolute;
		top: ${HEADER_SIZE}px;
		left: ${NAVIGATION_SIZE}px;
		right: 0;
		bottom: 0;

		@media ${WIDESCREEN} {
			left: ${WIDESCREEN_NAVIGATION_SIZE_CALC};
		}
	}
`;

const navOverlayClass = css`
	position: absolute;
	opacity: 0;
	transition: opacity ease-out 200ms;
	pointer-events: none;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	background-color: rgba(0, 0, 0, 0.4);
	z-index: 25;

	> label {
		position: absolute;
		display: block;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
	}
`;

const menuCheckboxClass = css`
	visibility: hidden;
	pointer-events: none;

	@media ${MOBILE} {
		/* By default */
		& ~ .${navigationClass} {
			width: ${MOBILE_NAVIGATION_SIZE}px;
			max-width: 100%;
			transform: translateX(-100%);
			transition: transform ease-out 200ms;
			background-color: ${MainTheme.mainBackgroundColor};
			border-right: 1px solid ${MainTheme.mainBorderColor};
		}

		& ~ .${mainClass} {
			left: 0px;
		}

		/* After the user clicks */
		&:checked ~ .${navigationClass} {
			display: block;
			transform: none;
		}

		&:checked ~ .${navOverlayClass} {
			pointer-events: auto;
			opacity: 1;
		}
	}
`;

const loadingAnimation = keyframes`
	0% {
		transform: translateX(-100%);
	}
	25% {
		transform: translateX(-50%);
	}
	50% {
		transform: translateX(0%);
	}
	75% {
		transform: translateX(50%);
	}
	100% {
		transform: translateX(100%);
	}
`;

const pageLoadingClass = css`
	position: absolute;
	pointer-events: none;
	top: ${HEADER_SIZE}px;
	left: 0;
	right: 0;
	height: 3px;
	transform: translateY(-100%);
	z-index: 125;
	overflow: hidden;
`;

const pageLoadingBarClass = css`
	position: absolute;
	top: 0;
	left: 0;
	height: 3px;
	width: 100%;
	transform: translateX(-100%);
	will-change: transform;
`;

const pageLoadingProgressClass = css`
	background: linear-gradient(
		to right,
		rgba(255, 255, 255, 0),
		#5172af 10%,
		#4e91c2 20%,
		#57b9c4 80%,
		#5172af 90%,
		rgba(255, 255, 255, 0)
	);

	animation: ${loadingAnimation} 1s ease-in-out infinite;

	@media (max-width: 450px) {
		animation: ${loadingAnimation} 750ms ease-in-out infinite;
	}
`;

export const PageLoading = () => {
	const store = useStore();
	const isLoading = store.state.pageDetails.displayPageLoading;
	return (
		<div class={pageLoadingClass}>
			<div
				class={`${pageLoadingBarClass} ${
					isLoading ? pageLoadingProgressClass : ``
				}`}
			/>
		</div>
	);
};

export const PageLayout: FunctionComponent = () => {
	return (
		<div class={layoutClass}>
			<input class={menuCheckboxClass} type="checkbox" id="menu" />
			<div class={navOverlayClass}>
				<label for="menu" />
			</div>
			<header class={headerClass}>
				<TopBar />
			</header>
			<PageLoading />
			<nav class={navigationClass}>
				<PageNavigation />
			</nav>
			<MainContent>
				<div class={mainContentClass}>
					<PageContent />
				</div>
			</MainContent>
		</div>
	);
};

const MainContent: FunctionComponent = ({ children }) => {
	const store = useStore();
	const mainRef = useRef<HTMLElement>(null);

	useLayoutEffect(() => {
		const main = mainRef.current;
		if (!main) {
			return () => {};
		}
		const onScroll = () => {
			store.actions.locationActions.saveScroll(main.scrollTop);
		};
		main.addEventListener("scroll", onScroll);
		return () => {
			main.removeEventListener("scroll", onScroll);
		};
	}, []);

	return (
		<main id="main-content" class={mainClass} ref={mainRef}>
			{children}
		</main>
	);
};

const topBarClass = css`
	display: flex;
	justify-content: space-between;

	@media ${WIDESCREEN} {
		position: absolute;
		left: 50%;
		width: ${WIDESCREEN_BREAKPOINT}px;
		transform: translateX(-50%);
	}
`;

const fullLogoClass = css`
	display: inline-block;
	color: #f0e5d7;
	transition: color linear 150ms;
	vertical-align: middle;

	&:hover {
		color: ${MainTheme.themeColor};
	}
`;

const burgerMenuClass = css`
	display: inline-block;
	vertical-align: middle;
	margin-right: 8px;
	cursor: pointer;

	@media ${NON_MOBILE} {
		display: none;
	}
`;

const logoClass = css`
	display: inline-block;
	width: 40px;
	height: 40px;
	background-image: url(/fluidstate-100.png);
	background-size: cover;
	background-repeat: no-repeat;
	vertical-align: middle;
`;

const logoLabelClass = css`
	display: inline-block;
	font-family: "Averia Gruesa Libre", sans-serif;
	vertical-align: middle;
	font-size: 20px;
	margin-left: 6px;
`;

const gitlabLogoClass = css`
	display: block;
	margin: 0px 8px;
	width: 40px;
	height: 40px;
	background-image: url(/gitlab-logo.png);
	background-size: cover;
	background-repeat: no-repeat;
`;

const TopBar = () => {
	return (
		<div class={topBarClass}>
			<div>
				<label for="menu">
					<Burger className={burgerMenuClass} />
				</label>
				<PageLink page="intro-main" class={fullLogoClass} onClick={toggleMenu}>
					<div class={logoClass} />
					<h1 class={logoLabelClass}>Fluid State</h1>
				</PageLink>
			</div>
			<Link href="https://gitlab.com/fluidstate" class={gitlabLogoClass} />
		</div>
	);
};

const pageNavigationItemClass = css`
	display: block;
`;

const pageNavigationLinkClass = css`
	display: block;
	text-decoration: none;
	font-size: 15px;
	padding: 3px 30px;

	&:first-of-type {
		padding-top: 6px;
	}

	&:last-of-type {
		padding-bottom: 6px;
	}

	&:hover {
		text-decoration: underline;
	}
`;

const toggleMenu = () => {
	(document.getElementById("menu")! as HTMLInputElement).checked = false;
};

const PageNavigationItem = ({
	page,
	children,
}: {
	page: PageName;
	children: ComponentChildren;
}) => {
	return (
		<li class={pageNavigationItemClass}>
			<PageLink
				page={page}
				class={pageNavigationLinkClass}
				onClick={toggleMenu}
			>
				{children}
			</PageLink>
		</li>
	);
};

const pageNavigationClass = css`
	display: inline-block;
	margin: 6px 0px;
	text-align: start;
	width: ${NAVIGATION_SIZE}px;
	max-width: 100%;

	@media ${MOBILE} {
		width: ${MOBILE_NAVIGATION_SIZE}px;
	}

	> h1 {
		margin: 18px 18px 6px 18px;
	}

	> :last-child {
		margin-bottom: 36px;
	}
`;

const PageNavigation = () => {
	return (
		<div class={pageNavigationClass}>
			<h1>Introduction</h1>
			<ul>
				<PageNavigationItem page="intro-main">
					Getting started
				</PageNavigationItem>
				<PageNavigationItem page="intro-react">
					Using with React
				</PageNavigationItem>
				<PageNavigationItem page="intro-preact">
					Using with Preact
				</PageNavigationItem>
			</ul>
			<h1>Reactivity</h1>
			<ul>
				<PageNavigationItem page="reactivity-why-reactivity">
					Why reactivity?
				</PageNavigationItem>
				<PageNavigationItem page="reactivity-declarative-vs-imperative">
					Declarative vs. imperative code
				</PageNavigationItem>
				<PageNavigationItem page="reactivity-fluidstate-goal">
					Goal of Fluid State
				</PageNavigationItem>
				<PageNavigationItem page="reactivity-fluidstate-vs-reducers">
					Fluid State vs. Reducers
				</PageNavigationItem>
				<PageNavigationItem page="reactivity-fluidstate-vs-solid">
					Fluid State vs. Solid Signals
				</PageNavigationItem>
				<PageNavigationItem page="reactivity-fluidstate-vs-mobx">
					Fluid State vs. MobX
				</PageNavigationItem>
				<PageNavigationItem page="reactivity-event-systems-vs-reactivity">
					Event systems vs. reactivity
				</PageNavigationItem>
			</ul>
			<h1>Fluid State</h1>
			<ul>
				<PageNavigationItem page="api-reactive-objects">
					Reactive objects
				</PageNavigationItem>
				<PageNavigationItem page="api-reactive-effects">
					Reactive effects
				</PageNavigationItem>
				<PageNavigationItem page="api-primitives-observed-computed">
					Primitives: Observed, Computed
				</PageNavigationItem>
				<PageNavigationItem page="api-primitives-actions-effects">
					Primitives: Actions, Effects
				</PageNavigationItem>
				<PageNavigationItem page="api-batching">Batching</PageNavigationItem>
				<PageNavigationItem page="api-update-scheduling">
					Update scheduling
				</PageNavigationItem>
				<PageNavigationItem page="api-reactive-inert-utilities">
					Reactive and inert utilities
				</PageNavigationItem>
			</ul>
			<h1>How to build reactive apps</h1>
			<ul>
				<PageNavigationItem page="build-apps-overview">
					Overview
				</PageNavigationItem>
				<PageNavigationItem page="build-apps-separate-view-data">
					Separate view and data
				</PageNavigationItem>
				<PageNavigationItem page="build-apps-global-vs-local">
					Global vs. local state
				</PageNavigationItem>
				<PageNavigationItem page="build-apps-modular-state">
					Modular state
				</PageNavigationItem>
				<PageNavigationItem page="build-apps-framework-independence">
					Framework independence
				</PageNavigationItem>
				<PageNavigationItem page="build-apps-presentational-only">
					Write only presentational components
				</PageNavigationItem>
				<PageNavigationItem page="build-apps-ssr-csr">
					Server and client side rendering
				</PageNavigationItem>
			</ul>
			<h1>Interactive examples</h1>
			<ul>
				<PageNavigationItem page="intro-main">
					Your playground!
				</PageNavigationItem>
				<PageNavigationItem page="example-todo">To-do app</PageNavigationItem>
				<PageNavigationItem page="example-time-travel">
					Time travel
				</PageNavigationItem>
				<PageNavigationItem page="example-form-validation">
					Form validation
				</PageNavigationItem>
				<PageNavigationItem page="example-data-filtering">
					Data filtering
				</PageNavigationItem>
				<PageNavigationItem page="example-data-filtering">
					Dynamic layout
				</PageNavigationItem>
				<PageNavigationItem page="example-quiz-app">
					Quiz app
				</PageNavigationItem>
				<PageNavigationItem page="example-puzzle-game">
					Puzzle game
				</PageNavigationItem>
				<PageNavigationItem page="example-mini-paint">
					Mini paint
				</PageNavigationItem>
			</ul>
			<h1>Miscellaneous</h1>
			<ul>
				<PageNavigationItem page="misc-inspiration">
					Inspiration
				</PageNavigationItem>
				<PageNavigationItem page="misc-credits">Credits</PageNavigationItem>
				<PageNavigationItem page="misc-license">License</PageNavigationItem>
			</ul>
		</div>
	);
};

export const PageContent: FunctionComponent = () => {
	const store = useStore();
	const ReactiveRoot = store.state.pageDetails.displayedPage.Root;
	const Root = getInert(ReactiveRoot) ?? ReactiveRoot;
	return <Root />;
};
