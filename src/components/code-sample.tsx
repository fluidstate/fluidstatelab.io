import CodeMirror from "@uiw/react-codemirror";
import { javascript } from "@codemirror/lang-javascript";
import type Babel from "@babel/standalone";
import { css } from "../common/css";
import {
	useCallback,
	useEffect,
	useMemo,
	useRef,
	useState,
} from "preact/hooks";
import { useDebounced } from "../common/use-debounced";
import { tokyoNight } from "@uiw/codemirror-theme-tokyo-night";
import { useStateWithRef } from "../common/use-state-with-ref";
import { MOBILE, NON_MOBILE } from "../common/media-const";
import { win } from "../common/window";

interface IframeWin extends Window {
	isLoaded: boolean;
	runCode: (babel: typeof Babel, code: string) => void;
	Babel: {
		transform: (code: string, options: object) => { code: string };
	};
}

export enum SampleType {
	FluidState,
	FluidStatePreact,
	FluidStateReact,
}

const codeSampleClass = css`
	.cm-scroller {
		line-height: 1.6;
	}

	@media ${NON_MOBILE} {
		display: flex;
		flex-direction: row;
		align-items: stretch;

		> * {
			flex: 1 1 0;
			width: 0;
		}

		> iframe {
			flex: 0.75 1 0;
		}
	}

	@media ${MOBILE} {
		> iframe {
			min-height: 300px;
			width: 100%;
		}
	}
`;

const codeSampleHiddenClass = css`
	display: none;
`;

const ensureEmptyLine = (code: string) => {
	return /\n$/.test(code) ? code : `${code}\n`;
};

export const CodeSampleInitial = ({
	codeHeight,
	initialCode,
}: {
	codeHeight: string;
	initialCode: string;
}) => {
	return <code style={{ height: codeHeight }}>{initialCode}</code>;
};

export const CodeSample = ({
	initialCode: untransformedInitialCode,
	height = "auto",
}: {
	initialCode: string;
	height?: string;
}) => {
	const initialCode = ensureEmptyLine(untransformedInitialCode);
	let codeHeight = height;
	if ((initialCode.match(/\n/g)?.length || 0) > 22 && codeHeight === "auto") {
		codeHeight = "600px";
	}

	const [isInitialRender, setInitialRender] = useState(true);
	const onEditorCreated = () => {
		if (isInitialRender) {
			setInitialRender(false);
		}
	};

	const extensions = useMemo(
		() => [javascript({ jsx: true, typescript: true })],
		[]
	);

	const [code, setUndebouncedCode, codeRef] = useStateWithRef(initialCode);

	const type =
		code.indexOf("fluidstate-preact") >= 0
			? SampleType.FluidStatePreact
			: code.indexOf("fluidstate-react") >= 0
			? SampleType.FluidStateReact
			: SampleType.FluidState;

	const setCode = useDebounced(500, setUndebouncedCode);
	const iframeRef = useRef<HTMLIFrameElement>();
	const isFirstRender = useRef(true);

	const runCode = useCallback(() => {
		if (iframeRef.current) {
			try {
				const iframeWindow = iframeRef.current
					.contentWindow as IframeWin | null;
				if (iframeWindow && "isLoaded" in iframeWindow) {
					iframeWindow.runCode(win.Babel, code);
				}
			} catch (e) {
				console.error(e);
			}
		}
	}, [code]);

	useEffect(() => {
		if (!isFirstRender.current || code !== initialCode) {
			runCode();
		}
		if (isFirstRender.current) {
			isFirstRender.current = false;
		}
	}, [code]);

	const iframeURL =
		type === SampleType.FluidState
			? `/playground-fluidstate.html`
			: type === SampleType.FluidStatePreact
			? `/playground-preact.html`
			: `/playground-react.html`;

	const iframeRefCallback = useCallback(
		(iframe: HTMLIFrameElement | null) => {
			if (iframe) {
				iframe.addEventListener("load", runCode);
				iframeRef.current = iframe;
				const iframeWindow = iframe.contentWindow as IframeWin | null;
				if (iframeWindow && "isLoaded" in iframeWindow) {
					iframeWindow.runCode(win.Babel, codeRef.current);
				}
			} else {
				if (iframeRef.current) {
					iframeRef.current.removeEventListener("load", runCode);
					iframeRef.current = undefined;
				}
			}
		},
		[iframeURL]
	);

	return (
		<>
			{isInitialRender ? (
				<CodeSampleInitial codeHeight={codeHeight} initialCode={initialCode} />
			) : null}
			<div
				class={`${codeSampleClass} ${
					isInitialRender ? codeSampleHiddenClass : ""
				}`}
			>
				<CodeMirror
					value={initialCode}
					theme={tokyoNight}
					height={codeHeight}
					extensions={extensions}
					onChange={setCode}
					basicSetup={{
						autocompletion: false,
						foldGutter: false,
					}}
					onCreateEditor={onEditorCreated}
				/>
				<iframe
					key={iframeURL}
					src={iframeURL}
					ref={iframeRefCallback}
					style={{ height }}
				/>
			</div>
		</>
	);
};
