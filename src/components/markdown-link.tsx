import { HTMLAttributes } from "preact/compat";
import { Pages } from "../common/pages-generated";
import { PageName } from "../common/pages-generated";
import { PageLink } from "./page-link";
import { Link } from "./link";

export const MarkdownLink = (
	props: HTMLAttributes<HTMLAnchorElement> & {
		href?: string;
		isHeaderLink?: boolean;
	}
) => {
	const isFootnote = props.href && /^#[0-9]+$/.test(props.href);
	if (props.href && /.md$/.test(props.href)) {
		const pageName = props.href.replace(/.md$/, "");
		if (!(pageName in Pages)) {
			throw new Error(
				`Page ${pageName} was used in markdown link but such page does not exist`
			);
		}
		return <PageLink {...props} page={pageName as PageName} />;
	}
	return (
		<Link
			{...props}
			style={props.isHeaderLink ? { textUnderlineOffset: "5px" } : {}}
			decorators={[
				isFootnote || props.isHeaderLink ? "no-underline" : null,
				props.isHeaderLink ? "hover-underline" : null,
			]}
		/>
	);
};
