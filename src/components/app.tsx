import { FunctionComponent } from "preact";
import { StoreContext } from "../common/store-context";
import { useState } from "preact/hooks";
import { createStore } from "../common/store";
import { PageLayout } from "./page-layout";
import { URL } from "../common/location";
import { Page } from "../common/page";

export const App: FunctionComponent<{ url?: URL; page: Page }> = ({
	url,
	page,
}) => {
	const [store] = useState(() => createStore({ url, page }));
	return (
		<StoreContext.Provider value={store}>
			<PageLayout />
		</StoreContext.Provider>
	);
};
