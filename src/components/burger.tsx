import { css } from "../common/css";
import { MainTheme } from "../common/theme";

const burgerLineClass = css`
	margin-top: 6px;
	height: 2px;
	width: 24px;
	border-radius: 1px;

	&:first-of-type {
		margin-top: 3px;
	}
`;

const burgerClass = css`
	width: 40px;
	height: 40px;
	padding: 8px;

	.${burgerLineClass} {
		background-color: ${MainTheme.mainTextColor};
		transition: background-color linear 150ms;
	}

	&:hover {
		.${burgerLineClass} {
			background-color: ${MainTheme.themeColor};
		}
	}
`;

export const Burger = ({ className }: { className?: string }) => {
	return (
		<div class={`${burgerClass} ${className || ""}`}>
			<div class={burgerLineClass}></div>
			<div class={burgerLineClass}></div>
			<div class={burgerLineClass}></div>
		</div>
	);
};
