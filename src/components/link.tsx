import { FunctionComponent } from "preact";
import { HTMLAttributes } from "preact/compat";
import { css } from "../common/css";
import { useStore } from "../common/store-context";
import { MainTheme } from "../common/theme";

const linkClass = css`
	all: unset;
	cursor: pointer;
	text-decoration: underline;
	transition: color linear 75ms;
	text-underline-offset: 2px;

	&:hover {
		color: ${MainTheme.themeColor};
	}
`;

const noUnderlineLinkClass = css`
	text-decoration: none;
`;

const hoverUnderlineLinkClass = css`
	&:hover {
		text-decoration: underline;
	}
`;

export function isLinkExternal(href: string) {
	return /^[a-zA-Z0-9]+?\:\/\//.test(href);
}

export const Link: FunctionComponent<
	HTMLAttributes<HTMLAnchorElement> & {
		href?: string;
		decorators?: Array<"no-underline" | "hover-underline" | null>;
	}
> = (props) => {
	const store = useStore();
	const newProps = { ...props };
	if (props.href && isLinkExternal(props.href)) {
		newProps.rel = "noopener noreferrer";
		newProps.target = "_blank";
	} else {
		newProps.onClick = function (e) {
			if (props.href) {
				e.preventDefault();
				store.actions.locationActions.push(props.href);
			}
			if (props.onClick) {
				return props.onClick.call(this, e);
			}
		};
	}
	return (
		<a
			{...newProps}
			class={`${newProps.class || newProps.className || ""} ${linkClass} ${
				props.decorators?.includes("no-underline") ? noUnderlineLinkClass : ""
			} ${
				props.decorators?.includes("hover-underline")
					? hoverUnderlineLinkClass
					: ""
			}`}
		/>
	);
};
