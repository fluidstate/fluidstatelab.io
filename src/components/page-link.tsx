import { FunctionComponent } from "preact";
import { PageName, Pages } from "../common/pages-generated";
import { getUrlFromPageUrl } from "../common/page-details";
import { HTMLAttributes } from "preact/compat";
import { Link } from "./link";

export const PageLink: FunctionComponent<
	{ page: PageName } & HTMLAttributes<HTMLAnchorElement>
> = ({ page, ...props }) => {
	const { url } = Pages[page];
	return <Link {...props} href={getUrlFromPageUrl(url)} />;
};
