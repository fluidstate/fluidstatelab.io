import MarkdownLibrary from "markdown-to-jsx";
import { CodeSample } from "./code-sample";
import { MarkdownLink } from "./markdown-link";
import { css } from "../common/css";
import { HTMLAttributes } from "preact/compat";
import { FunctionComponent, h } from "preact";

const defaultFlatMargin = 18;

const markdownClass = css`
	> *:not(div),
	> div > *:not(div),
	> div > div > *:not(div) {
		margin: 20px ${defaultFlatMargin}px;
	}
	sup {
		vertical-align: super;
		font-size: 13px;
	}
	h1,
	h2,
	h3,
	h4,
	h5,
	h6 {
		line-height: 1.15;
	}
	h1 {
		font-size: 46px;
		margin-left: ${defaultFlatMargin - 4}px !important;
		margin-right: ${defaultFlatMargin - 4}px !important;
	}
	h2 {
		font-size: 41px;
		margin-left: ${defaultFlatMargin - 4}px !important;
		margin-right: ${defaultFlatMargin - 4}px !important;
	}
	h3 {
		font-size: 36px;
		margin-left: ${defaultFlatMargin - 3}px !important;
		margin-right: ${defaultFlatMargin - 3}px !important;
	}
	h4 {
		font-size: 31px;
		margin-left: ${defaultFlatMargin - 3}px !important;
		margin-right: ${defaultFlatMargin - 3}px !important;
	}
	h5 {
		font-size: 26px;
		margin-left: ${defaultFlatMargin - 2}px !important;
		margin-right: ${defaultFlatMargin - 2}px !important;
	}
	h6 {
		font-size: 21px;
		margin-left: ${defaultFlatMargin - 2}px !important;
		margin-right: ${defaultFlatMargin - 2}px !important;
	}
	code {
		background-color: #1a1b26;
		color: white;
		padding-left: 4px;
		padding-right: 4px;
		border-radius: 3px;
	}
	pre > code {
		display: block;
		font-family: monospace;
		overflow: auto;
		padding: 8px 12px;
	}
	ol,
	ul {
		margin-left: 50px !important;
	}
	ol > li {
		list-style-type: decimal;
	}
	ul > li {
		list-style-type: disc;
	}
	strong {
		font-weight: 700;
	}
	img {
		position: relative;
		left: 50%;
		transform: translateX(-50%);
		max-width: 100%;
	}
`;

const MarkdownImage = (props: HTMLAttributes<HTMLImageElement>) => {
	const src = props.src as string | undefined;
	let style: (typeof props)["style"];
	if (src && src.indexOf("xkcd.com") >= 0) {
		style = { filter: "sepia(1) invert(91%)" };
	}
	return <img style={style} {...props} />;
};

type HNum = "h1" | "h2" | "h3" | "h4" | "h5" | "h6";

const createHeaderComponent = (tag: HNum) => {
	const Component: FunctionComponent = ({
		children,
		...props
	}: HTMLAttributes<HTMLElement>) => {
		children = props.id ? (
			<MarkdownLink href={`#${props.id}`} isHeaderLink>
				{children}
			</MarkdownLink>
		) : (
			children
		);
		return h(tag, props, children);
	};
	return Component;
};

export const Markdown = ({ children }: { children: string }) => {
	return (
		<div class={markdownClass}>
			<MarkdownLibrary
				options={{
					overrides: {
						a: MarkdownLink,
						code: (props: { className?: string; children: string }) => {
							const language =
								props.className && props.className.replace(/^lang-/, "");
							return language === "js" ? (
								<CodeSample initialCode={props.children} />
							) : (
								<code {...props} />
							);
						},
						img: MarkdownImage,
						h1: createHeaderComponent("h1"),
						h2: createHeaderComponent("h2"),
						h3: createHeaderComponent("h3"),
						h4: createHeaderComponent("h4"),
						h5: createHeaderComponent("h5"),
						h6: createHeaderComponent("h6"),
					},
				}}
			>
				{children}
			</MarkdownLibrary>
		</div>
	);
};
