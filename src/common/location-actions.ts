import { createDebouncedFn } from "./debounced";
import { Location } from "./location";
import { scrollMainTo, scrollToId } from "./location-scroll-utils";

export type LocationActions = ReturnType<typeof createLocationActions>;

export const createLocationActions = (location: Location) => {
	type State =
		| null
		| undefined
		| { mainScrollTop?: number; idToScrollTo?: string };

	const onLocationChange = (
		e: null | {
			state: State;
		}
	) => {
		if (e?.state?.mainScrollTop != null) {
			scrollMainTo(e.state.mainScrollTop);
		}
		if (e?.state?.idToScrollTo) {
			scrollToId(e.state.idToScrollTo);
		}
		location.url.hash = window.location.hash;
		location.url.pathname = window.location.pathname;
		location.url.search = window.location.search;
	};

	const isEqualToCurrentState = (url: URL, newState: State) => {
		const windowState = window.history.state as State;
		return (
			`${url}` === `${window.location}` &&
			windowState?.idToScrollTo === newState?.idToScrollTo &&
			windowState?.mainScrollTop === newState?.mainScrollTop
		);
	};

	const getURL = (url: string) => {
		return new URL(url, window.location.origin);
	};

	const getState = (url: URL): State => {
		if (url.hash) {
			return { idToScrollTo: url.hash.substring(1) };
		}
		return { mainScrollTop: 0 };
	};

	const push = (urlStr: string) => {
		const url = getURL(urlStr);
		const state = getState(url);
		if (!isEqualToCurrentState(url, state)) {
			saveScrollInBrowser.callImmediately();
			window.history.pushState(state, "", url);
			onLocationChange({ state });
		}
	};

	const replace = (urlStr: string) => {
		const url = getURL(urlStr);
		const state = getState(url);
		if (!isEqualToCurrentState(url, state)) {
			saveScrollInBrowser.callImmediately();
			window.history.replaceState(state, "", url);
			onLocationChange({ state });
		}
	};

	const go = (steps: number) => {
		saveScrollInBrowser.callImmediately();
		window.history.go(steps);
		onLocationChange(null);
	};

	const goBack = () => {
		saveScrollInBrowser.callImmediately();
		window.history.back();
		onLocationChange(null);
	};

	const goForward = () => {
		saveScrollInBrowser.callImmediately();
		window.history.forward();
		onLocationChange(null);
	};

	const redirect = (url: string) => {
		saveScrollInBrowser.callImmediately();
		window.location.href = url;
	};

	const saveScrollInBrowser = createDebouncedFn(
		100,
		(mainScrollTop: number) => {
			window.history.replaceState({ mainScrollTop }, "", `${window.location}`);
		}
	);

	const saveScroll = (mainScrollTop: number) => {
		saveScrollInBrowser.debounced(mainScrollTop);
	};

	return {
		go,
		goBack,
		goForward,
		push,
		replace,
		redirect,
		onLocationChange,
		saveScroll,
	};
};
