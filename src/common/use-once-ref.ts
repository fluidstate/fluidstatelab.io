import { useRef } from "preact/hooks";

export const useOnceRef = <T>(fn: () => T) => {
	const isInitialized = useRef(false);
	const value = useRef(isInitialized.current ? (null as T) : fn());
	isInitialized.current = true;
	return value;
};
