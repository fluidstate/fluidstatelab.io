import { useEffect } from "preact/hooks";
import { useOnceRef } from "./use-once-ref";
import { createDebouncedFn } from "./debounced";

/**
 * Hook for creating a debounced function
 */
export const useDebounced = <T extends unknown[]>(
	delay: number,
	fn: (...args: T) => void
): ((...args: T) => void) => {
	const debounced = useOnceRef(() => createDebouncedFn(delay, fn));

	useEffect(() => {
		return () => {
			debounced.current.cancel();
		};
	});

	return debounced.current.debounced;
};
