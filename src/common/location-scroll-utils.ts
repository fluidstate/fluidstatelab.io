let timeout1: any = null;

let timeout2: any = null;

export const cancelTimeouts = () => {
	if (timeout1 !== null) {
		clearTimeout(timeout1);
	}
	if (timeout2 !== null) {
		clearTimeout(timeout2);
	}
};

export const scrollToId = (id: string | null) => {
	if (id) {
		// Note: Chrome has some issues with "scrollIntoView", so
		// we will default to using "scrollTo" instead
		let element = document.getElementById(id);
		if (element?.offsetParent?.id === "main-content") {
			scrollMainTo(element.offsetTop);
		} else {
			cancelTimeouts();
			timeout1 = setTimeout(() => {
				timeout2 = setTimeout(() => {
					timeout1 = null;
					timeout2 = null;
					element = document.getElementById(id);
					element?.scrollIntoView({
						behavior: "smooth",
					});
				});
			});
		}
	}
};

export const scrollMainTo = (mainScrollTop: number) => {
	cancelTimeouts();
	timeout1 = setTimeout(() => {
		timeout2 = setTimeout(() => {
			timeout1 = null;
			timeout2 = null;
			document.getElementById("main-content")?.scrollTo({
				top: mainScrollTop,
				behavior: "smooth",
			});
		});
	});
};
