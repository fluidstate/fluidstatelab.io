import { createContext } from "preact";
import { Store } from "./store";
import { useContext } from "preact/hooks";
import { inertify } from "./inertify";

export const StoreContext = createContext<Store>(null as unknown as Store);
inertify(StoreContext.Provider);

export const useStore = () => useContext(StoreContext);
