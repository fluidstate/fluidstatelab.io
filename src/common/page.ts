import { FunctionComponent } from "preact";
import { Pages } from "./pages-generated";

export type Page = {
	name: keyof typeof Pages;
	title: string;
	url: string;
	Root: FunctionComponent;
};
