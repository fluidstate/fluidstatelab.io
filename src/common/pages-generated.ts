export type PageName = keyof typeof Pages;

// Note: this file is auto generated. Just run "yarn build"
export const Pages = {
	"api-batching": { title: "Fluid State - Batching", url: "api-batching" },
	"api-primitives-actions-effects": {
		title: "Fluid State - Primitives: Actions and Effects",
		url: "api-primitives-actions-effects",
	},
	"api-primitives-observed-computed": {
		title: "Fluid State - Primitives: Observed and Computed",
		url: "api-primitives-observed-computed",
	},
	"api-reactive-effects": {
		title: "Fluid State - Reactive effects",
		url: "api-reactive-effects",
	},
	"api-reactive-inert-utilities": {
		title: "Fluid State - Reactive and inert utilities",
		url: "api-reactive-inert-utilities",
	},
	"api-reactive-objects": {
		title: "Fluid State - Reactive objects",
		url: "api-reactive-objects",
	},
	"api-update-scheduling": {
		title: "Fluid State - Update scheduling",
		url: "api-update-scheduling",
	},
	"build-apps-framework-independence": {
		title: "Fluid State - Framework independence",
		url: "build-apps-framework-independence",
	},
	"build-apps-global-vs-local": {
		title: "Fluid State - Global vs. local state",
		url: "build-apps-global-vs-local",
	},
	"build-apps-modular-state": {
		title: "Fluid State - Modular state",
		url: "build-apps-modular-state",
	},
	"build-apps-overview": {
		title: "Fluid State - How to build reactive apps",
		url: "build-apps-overview",
	},
	"build-apps-presentational-only": {
		title: "Fluid State - Write only presentational components",
		url: "build-apps-presentational-only",
	},
	"build-apps-separate-view-data": {
		title: "Fluid State - Separate view and data",
		url: "build-apps-separate-view-data",
	},
	"build-apps-ssr-csr": {
		title: "Fluid State - Server and client side rendering",
		url: "build-apps-ssr-csr",
	},
	"example-data-filtering": {
		title: "Fluid State - Example: Data filtering",
		url: "example-data-filtering",
	},
	"example-dynamic-layout": {
		title: "Fluid State - Example: Dynamic layout",
		url: "example-dynamic-layout",
	},
	"example-form-validation": {
		title: "Fluid State - Example: Form validation",
		url: "example-form-validation",
	},
	"example-mini-paint": {
		title: "Fluid State - Example: Mini paint",
		url: "example-mini-paint",
	},
	"example-puzzle-game": {
		title: "Fluid State - Example: Puzzle game",
		url: "example-puzzle-game",
	},
	"example-quiz-app": {
		title: "Fluid State - Example: Quiz app",
		url: "example-quiz-app",
	},
	"example-time-travel": {
		title: "Fluid State - Example: Time travel",
		url: "example-time-travel",
	},
	"example-todo": {
		title: "Fluid State - Example: To-do app",
		url: "example-todo",
	},
	"intro-main": {
		title: "Fluid State - Simple, Powerful, Reactive State Management",
		url: "index.html",
	},
	"intro-preact": {
		title: "Fluid State - Fluid State with Preact",
		url: "intro-preact",
	},
	"intro-react": {
		title: "Fluid State - Fluid State with React",
		url: "intro-react",
	},
	"misc-credits": { title: "Fluid State - Credits", url: "misc-credits" },
	"misc-inspiration": {
		title: "Fluid State - Inspiration",
		url: "misc-inspiration",
	},
	"misc-license": { title: "Fluid State - License", url: "misc-license" },
	"not-found": { title: "Fluid State - Page not found", url: "not-found" },
	"reactivity-declarative-vs-imperative": {
		title: "Fluid State - Declarative vs. imperative code",
		url: "reactivity-declarative-vs-imperative",
	},
	"reactivity-event-systems-vs-reactivity": {
		title: "Fluid State - Event systems vs. reactivity",
		url: "reactivity-event-systems-vs-reactivity",
	},
	"reactivity-fluidstate-goal": {
		title: "Fluid State - Goal of Fluid State",
		url: "reactivity-fluidstate-goal",
	},
	"reactivity-fluidstate-vs-mobx": {
		title: "Fluid State - Fluid State vs. MobX",
		url: "reactivity-fluidstate-vs-mobx",
	},
	"reactivity-fluidstate-vs-reducers": {
		title: "Fluid State - Fluid State vs. Reducers",
		url: "reactivity-fluidstate-vs-reducers",
	},
	"reactivity-fluidstate-vs-solid": {
		title: "Fluid State - Fluid State vs. Solid Signals",
		url: "reactivity-fluidstate-vs-solid",
	},
	"reactivity-why-reactivity": {
		title: "Fluid State - Why reactivity?",
		url: "reactivity-why-reactivity",
	},
};
