import { createReaction } from "fluidstate";
import { Page } from "./page";
import { Pages, PageName } from "./pages-generated";
import { Location } from "./location";
import { isClientSide } from "./client-side";
import { LocationActions } from "./location-actions";

const TIME_BETWEEN_LOADING_START_AND_DISPLAY_MS = 400;

export type PageStatus =
	| {
			name: PageName;
			loaded: false;
			promise: Promise<Page> | null;
	  }
	| {
			name: PageName;
			loaded: true;
			page: Page;
	  };

export type PageDetails = {
	details: {
		[K in keyof typeof Pages]?: PageStatus;
	};

	displayedPage: Page;

	selectedPageName: PageName;

	selectedPageLoading: boolean;

	displayPageLoading: boolean;
};

export const createPageDetails = (
	location: Location,
	initialPage: Page
): PageDetails => {
	const details: PageDetails["details"] = {};

	details[initialPage.name] = {
		name: initialPage.name,
		loaded: true,
		page: initialPage,
	};

	return {
		details,

		displayedPage: initialPage,

		get selectedPageName() {
			const trimmedPathname = location.pathname
				.replace(/^\//, "")
				.replace(/\/$/, "");

			let selectedPageName: keyof typeof Pages = "not-found";

			if (trimmedPathname === "") {
				selectedPageName = "intro-main";
			}

			for (const name in Pages) {
				const pageName = name as keyof typeof Pages;
				const result = Pages[pageName];
				if (result.url === trimmedPathname) {
					selectedPageName = pageName;
				}
			}

			return selectedPageName;
		},

		get selectedPageLoading() {
			const selectedPageDetails = this.details[this.selectedPageName];
			return !!(
				selectedPageDetails?.loaded === false && selectedPageDetails?.promise
			);
		},

		displayPageLoading: false,
	};
};

export const getUrlFromPageUrl = (url: string) =>
	url === "index.html" ? "/" : `/${url}`;

export const createPageActions = (locationActions: LocationActions) => {
	return {
		openPage(pageName: PageName) {
			const { url } = Pages[pageName];
			locationActions.push(getUrlFromPageUrl(url));
		},
	};
};

export const createPageEffects = (pageDetails: PageDetails) => {
	let setDisplayPageLoadingTimeout: any;

	return {
		loadPage: createReaction(
			() => pageDetails.selectedPageName,
			(selectedPageName) => {
				if (!pageDetails.details[selectedPageName]) {
					pageDetails.details[selectedPageName] = {
						name: selectedPageName,
						loaded: false,
						promise: import(`../pages/${selectedPageName}.tsx`).then(
							({ page }) => {
								pageDetails.details[selectedPageName] = {
									loaded: true,
									name: selectedPageName,
									page,
								};
								return page;
							}
						),
					};
				}
			}
		),

		setDisplayedPage: createReaction(
			() => pageDetails.details[pageDetails.selectedPageName],
			(selectedPageDetails) => {
				if (selectedPageDetails && selectedPageDetails.loaded) {
					pageDetails.displayedPage = selectedPageDetails.page;
				}
			}
		),

		setTitle: createReaction(
			() => pageDetails.displayedPage.title,
			(title) => {
				if (isClientSide()) {
					document.title = title;
				}
			}
		),

		setDisplayPageLoading: createReaction(
			() => pageDetails.selectedPageLoading,
			(loading) => {
				if (loading && !setDisplayPageLoadingTimeout) {
					setDisplayPageLoadingTimeout = setTimeout(() => {
						setDisplayPageLoadingTimeout = null;
						pageDetails.displayPageLoading = true;
					}, TIME_BETWEEN_LOADING_START_AND_DISPLAY_MS);
				} else if (!loading) {
					clearTimeout(setDisplayPageLoadingTimeout);
					pageDetails.displayPageLoading = false;
				}
			}
		),
	};
};
