import { h, Fragment, hydrate } from "preact";
import { memo } from "preact/compat";
import { withFluidState } from "fluidstate-preact";
import { Page } from "./page";
import { isClientSide } from "./client-side";
import { App } from "../components/app";
import { isInert } from "./inertify";
import { injectReset } from "./reset";

let isInitialized = false;

export const declarePage = (page: Page) => {
	let isInitializing = false;
	if (!isInitialized) {
		isInitializing = true;
		isInitialized = true;
	}
	if (isInitializing) {
		injectReset();
	}
	if (isClientSide()) {
		if (isInitializing) {
			initPage(page);
		}
	}
	return page;
};

const initPage = (page: Page) => {
	defineGlobalPreact();
	hydrate(<App page={page} />, document.getElementById("root")!);
};

export const defineGlobalPreact = () => {
	const value = { h: createHyper(), Fragment };
	Object.freeze(value);
	Object.defineProperty(window, "preact", {
		configurable: false,
		enumerable: false,
		writable: false,
		value,
	});
};

const createHyper = (): typeof h => {
	const wrappers = new WeakMap<object, unknown>();
	return (type: any, props: unknown, ...children: unknown[]) => {
		if (typeof type === "function" && !isInert(type)) {
			let wrapper = wrappers.get(type);
			if (!wrapper) {
				wrapper = memo(withFluidState(type as any));
				wrappers.set(type, wrapper);
			}
			return h(wrapper as any, props as any, ...(children as any));
		}
		return h(type as any, props as any, ...(children as any));
	};
};
