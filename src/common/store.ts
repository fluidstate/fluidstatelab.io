import { createReactive } from "fluidstate";
import { URL, createLocation } from "./location";
import {
	createPageActions,
	createPageDetails,
	createPageEffects,
} from "./page-details";
import { Page } from "./page";
import { createLocationActions } from "./location-actions";
import { createLocationEffects } from "./location-effects";

export type Store = ReturnType<typeof createStore>;

export const createStore = ({ url, page }: { url?: URL; page: Page }) => {
	const location = createReactive(createLocation(url));
	const locationActions = createReactive(createLocationActions(location));
	const locationEffects = createLocationEffects(locationActions);

	const pageDetails = createReactive(createPageDetails(location, page));
	const pageActions = createReactive(createPageActions(locationActions));
	const pageEffects = createPageEffects(pageDetails);

	return {
		state: { location, pageDetails },
		actions: { locationActions, pageActions },
		stopReactions: { locationEffects, pageEffects },
	};
};
