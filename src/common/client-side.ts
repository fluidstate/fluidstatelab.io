let clientSide = true;

export const setClientSide = (newClientSide: boolean) => {
	clientSide = newClientSide;
};

export const isClientSide = () => clientSide;
