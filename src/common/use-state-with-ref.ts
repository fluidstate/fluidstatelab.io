import { useState, useRef } from "preact/hooks";

export const useStateWithRef = <T>(initialValue: T) => {
	const [value, setValue] = useState(initialValue);
	const valueRef = useRef(initialValue);
	const setValueAndRef = (newValue: T) => {
		setValue(newValue);
		valueRef.current = newValue;
	};
	return [value, setValueAndRef, valueRef] as const;
};
