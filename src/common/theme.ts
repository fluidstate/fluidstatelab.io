export const MainTheme = {
	themeColor: "#8adaec",
	mainBorderColor: "#14151b",
	mainBackgroundColor: "#161824",
	mainTextColor: "#e2ddd7",
};
