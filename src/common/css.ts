import { css as emotionCss } from "@emotion/css";
import { keyframes as emotionKeyframes } from "@emotion/css";

/**
 * Emotion has issues with having nested classnames if
 * those classnames are in the dynamic part of the template
 * string. This is a wrapper around @emotion/css to aleviate
 * these issues by putting everything in its static part.
 * This approach will probably break if we actually want some
 * dynamic css - however I am perfectly happy without it
 */
export const wrapCssInput =
	(fn: typeof emotionCss | typeof emotionKeyframes) =>
	(template: TemplateStringsArray, ...args: Array<unknown>) => {
		let myString = "";
		let myRawString = "";
		for (let i = 0; i < template.length; i++) {
			myString += template[i];
			myRawString += template.raw[i];
			if (i < args.length) {
				myString += args[i];
				myRawString += args[i];
			}
		}
		// @ts-ignore
		const newTemplate: Array<string> & { raw: string } = [myString];
		newTemplate.raw = myRawString;
		return fn(newTemplate);
	};

export const css = wrapCssInput(emotionCss);

export const keyframes = wrapCssInput(emotionKeyframes);
