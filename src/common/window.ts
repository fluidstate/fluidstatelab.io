import type Babel from "@babel/standalone";

interface Win extends Window {
	Babel: typeof Babel;
}

export const win = globalThis as unknown as Win;
