import { parse, ParsedQuery } from "query-string";

export type URL = {
	pathname: string;
	search: string;
	hash: string;
};

export type Location = {
	url: URL;
	pathname: string;
	query: ParsedQuery<string>;
};

export const createLocation = (url?: URL): Location => {
	return {
		url: url ?? {
			hash: window.location.hash,
			pathname: window.location.pathname,
			search: window.location.search,
		},

		get pathname() {
			return this.url.pathname;
		},

		get query(): ParsedQuery {
			const { search, hash } = this.url;
			const searchQuery: ParsedQuery = parse(search);
			const hashQuery: ParsedQuery = parse(hash);
			return {
				...searchQuery,
				...hashQuery,
			};
		},
	};
};
