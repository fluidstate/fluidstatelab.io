export const createDebouncedFn = <T extends unknown[]>(
	delay: number,
	fn: (...args: T) => void
) => {
	let timer: any = null;
	let lastArgs: T | null = null;

	const debounced = (...args: T) => {
		cancel();
		lastArgs = args;
		timer = setTimeout(() => callFn(args), delay);
	};

	const callFn = (args: T) => {
		timer = null;
		lastArgs = null;
		fn(...args);
	};

	const callImmediately = () => {
		if (lastArgs) {
			cancel();
			callFn(lastArgs);
		}
	};

	const cancel = () => {
		clearTimeout(timer);
	};

	return { debounced, callImmediately, cancel };
};
