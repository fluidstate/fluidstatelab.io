const inertSet = new WeakSet<object>();

export const inertify = (component: object) => {
	inertSet.add(component);
};

export const isInert = (component: object) => {
	return inertSet.has(component);
};
