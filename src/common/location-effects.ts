import { LocationActions } from "./location-actions";
import { isClientSide } from "./client-side";

export const createLocationEffects = (locationActions: LocationActions) => {
	if (isClientSide()) {
		window.addEventListener("popstate", locationActions.onLocationChange);
	}

	return {
		historyPop: () => {
			if (isClientSide()) {
				window.removeEventListener(
					"popstate",
					locationActions.onLocationChange
				);
			}
		},
	};
};
