export const MOBILE_BREAKPOINT = 850;
export const NON_MOBILE = `(min-width: ${MOBILE_BREAKPOINT}px)`;
export const MOBILE = `(max-width: ${MOBILE_BREAKPOINT}px) and (not ${NON_MOBILE})`;

export const WIDESCREEN_BREAKPOINT = 1700;
export const WIDESCREEN = `(min-width: ${WIDESCREEN_BREAKPOINT}px)`;
export const NON_WIDESCREEN = `(max-width: ${WIDESCREEN_BREAKPOINT}px) and (not ${WIDESCREEN})`;
