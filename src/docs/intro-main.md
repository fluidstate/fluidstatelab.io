# Fluid State

The aim of Fluid State is to make signal-based fine-grained reactivity as simple and clean as possible with minimal interface that is well aligned with the language and encourages best coding practices.

At the heart of JavaScript are JavaScript objects, arguably the most important value type[^1] in the language. At the heart of Fluid State are reactive objects.

```js
import { createReactive } from "fluidstate";
const visionary = createReactive({
  name: "Ada",
  lastName: "Lovelace",
  get fullName() {
    return `${this.name} ${this.lastName}`;
  },
});
// Prints "Ada Lovelace was a visionary"
console.log(`${visionary.fullName} was a visionary`);
```

Reactive objects preserve the full ergonomics and flexibility of regular JavaScript objects, but add reactivity, i.e. ability to run any code whenever parts of the object change.

```js
import { createReactive, createReaction } from "fluidstate";
const visionary = createReactive({
  name: "Ada",
  lastName: "Lovelace",
  get fullName() {
    return `${this.name} ${this.lastName}`;
  },
});
// Prints "Ada Lovelace was a visionary"
createReaction(
  () => visionary.fullName,
  (fullName) => {
    console.log(`${fullName} was a visionary`);
  }
);
// Prints "Luis Alvarez was a visionary"
visionary.name = "Luis";
visionary.lastName = "Alvarez";
```

`createReactive` and `createReaction` are the only two functions that you need for full featured reactivity. Fluid State is designed to eliminate boilerplate out of state management.

The current page is an overview that will give you sufficient understanding of Fluid State, and the rest of the pages will give context into why Fluid State is designed the way it is. This documentation will go in depth on how reactivity works, what reactive objects and reactions are, how they work under the hood, how updates and reactions are scheduled, how to build reactive apps in a modular and clean way, as well as provide a study of different reactive paradigms and event systems.

## Installation

NPM:

```
npm i fluidstate
```

Yarn:

```
yarn add fluidstate
```

## Use reactivity for state!

State management is one of the most diverse aspects of app development. Popular examples include [Redux](https://redux.js.org/), [Recoil](https://recoiljs.org/), [Jotai](https://jotai.org/), [Zustand](https://github.com/pmndrs/zustand), [Xstate](https://xstate.js.org), [MobX](https://mobx.js.org), as well as framework-specific state paradigms such as [Solid Signals](https://www.solidjs.com/guides/reactivity), [Vue Reactivity API](https://vuejs.org/guide/scaling-up/state-management.html) and [Svelte](https://kit.svelte.dev/docs/state-management). This is because state management is a non-trivial yet important problem.

![Famous XKCD Comic about standards](https://imgs.xkcd.com/comics/standards.png)

It is a reasonable observation that any large enough app benefits from a well-organized shared state management. However, once you pick a library, experience shows that:

1. A big portion of the app's business logic becomes tied and entirely dependent on the concepts of your state management library
2. All state management libraries have their own quirks, often causing the codebase to acquire workarounds and become unruly
3. Interfaces of state management libraries come with their own, often significant, boilerplate and restrictions, adding a multiplier to the time of implementation

Fluid State's goal is and will always be to reduce these problems by:

1. Advocating for use of simple JavaScript objects: your state is always going to consist of objects you create and mutate yourself, the way you want
2. Being unopinionated: it is up to you when to create the reactive state and how to split it. The majority of quirks with state management will be your own, fixable quirks
3. Having a small, language-grounded interface with fewer to no restrictions on how you use it: Fluid State's job is to simply provide a clear, clean and minimal interface and give recommendations on how to use it

And most importantly, Fluid State uses the concept of fine-grained reactivity and automatic dependency tracking. This concept is congruent with splitting code into smaller, loosely dependent pieces of data and primarily declarative logic. In practice, it makes writing and reading code significantly simpler. Here's the gist of what it's like to create and manage state with Fluid State (using React just as an example):

```js
import { createReactive, customizeSchedulerConfig } from "fluidstate";
import { withFluidState } from "fluidstate-react";
import { useState } from "react";
// Creating your own data
const createTodoData = () => ({
  todos: [],
});
// Creating your own actions
const createTodoActions = (todoData) => ({
  addTodo(text) {
    todoData.todos.push({ text, isDone: false });
  },
  removeTodo(index) {
    todoData.todos.splice(index, 1);
  },
  toggleTodoCompleted(index) {
    todoData.todos[index].isDone = !todoData.todos[index].isDone;
  },
});
// Wrap your functional components with `withFluidState` HOC or use `useFluidState` hook
export const App = withFluidState(() => {
  // Wrap your own objects / actions with `createReactive`. To share state with
  // downstream components, simply put your reactive objects into React Context
  const [todoData] = useState(() => createReactive(createTodoData()));
  const [todoActions] = useState(() =>
    createReactive(createTodoActions(todoData))
  );
  return (
    <ul>
      <li>
        Add To-Do:{" "}
        <input
          type="text"
          onKeyDown={(e) => {
            if (e.key === "Enter") {
              todoActions.addTodo(e.currentTarget.value);
              e.currentTarget.value = "";
            }
          }}
        />
      </li>
      {todoData.todos.map((todo, i) => (
        <li key={i}>
          <input
            type="checkbox"
            id={`todo-${i}`}
            checked={todo.isDone}
            onChange={() => todoActions.toggleTodoCompleted(i)}
            style={{ verticalAlign: "middle" }}
          />
          <label
            for={`todo-${i}`}
            style={{
              textDecoration: todo.isDone ? "line-through" : "none",
              verticalAlign: "middle",
              margin: "0px 4px",
            }}
          >
            {todo.text}
          </label>
          <button
            onClick={() => todoActions.removeTodo(i)}
            style={{ verticalAlign: "middle" }}
          >
            Remove
          </button>
        </li>
      ))}
    </ul>
  );
});
```

## Framework support

First-class Fluid State support is available for

- **React**: [See documentation for React](intro-react.md) — [See NPM for React](https://www.npmjs.com/package/fluidstate-react)
- **Preact**: [See documentation for Preact](intro-preact.md) — [See NPM for Preact](https://www.npmjs.com/package/fluidstate-preact)

## Reactive objects

Reactive objects can be any combination of plain JS objects, arrays, sets and maps. Their deeply nested reactivity is fully supported.

### Sets:

```js
import { createReactive, createReaction } from "fluidstate";
const veggiePizzas = createReactive(new Set(["Margherita", "Mediterranean"]));
// Prints "Sorry, no Garden pizza yet"
createReaction(
  () => veggiePizzas.has("Garden"),
  (hasGardenPizza) =>
    console.log(
      hasGardenPizza ? "We've got Garden pizza!" : "Sorry, no Garden pizza yet"
    )
);
// Prints "We've got Garden pizza!"
veggiePizzas.add("Garden");
```

### Maps:

```js
import { createReactive, createReaction } from "fluidstate";
const gameScores = createReactive(
  new Map([
    ["Eugene", 123],
    ["Baradun", 345],
    ["Bodger", 234],
  ])
);
// Prints "No score yet!"
createReaction(
  () => gameScores.get("Baelin"),
  (baelinScore) =>
    console.log(
      baelinScore == null ? "No score yet!" : `Baelin got: ${baelinScore}`
    )
);
// Prints "Baelin got: 600"
gameScores.set("Baelin", 600);
```

### Arrays:

```js
import { createReactive, createReaction } from "fluidstate";
const nolanFilmsInStock = createReactive([
  "Interstellar",
  "Inception",
  "The Dark Knight",
]);
// Prints "We have 3 Nolan's films"
createReaction(
  () => nolanFilmsInStock.length,
  (nolanFilmsCount) => console.log(`We have ${nolanFilmsCount} Nolan's films`)
);
// Prints "We have 4 Nolan's films"
nolanFilmsInStock.push("Tenet");
```

### Plain objects

Plain reactive objects are a little special. Generic objects can include three different kinds of properties:

1. **Observed values**: simple, independent values of any type
2. **Computed values**: values derived from other observed and/or computed values
3. **Actions**: functions that may modify any number of observed values in one go

Here's an example that contains all of them, but we will go into more detail on each of them in a moment:

```js
import { createReactive, createReaction } from "fluidstate";
const person = createReactive({
  // Observed properties:
  name: "Nicola Geiger",
  children: ["Andrea Geiger", "Vanessa Fabrega"],
  grandchildren: [],
  // Computed property:
  get descendantsCount() {
    return this.children.length + this.grandchildren.length;
  },
  // Action:
  addGrandchild(name) {
    this.grandchildren.push(name);
  },
});
// Prints: "Nicola Geiger has 2 descendants"
createReaction(
  () => ({ name: person.name, descendantsCount: person.descendantsCount }),
  ({ name, descendantsCount }) =>
    console.log(`${name} has ${descendantsCount} descendants`)
);
// Prints: "Nicola Geiger has 3 descendants"
person.addGrandchild("Alexa Fabrega-Geiger");
```

#### Observed properties

Observed properties are at the root of reactivity. Changes to observed properties are propagated to computed properties and reactions that depend on them.

```js
import { createReactive, createReaction } from "fluidstate";
const rectangle = createReactive({
  // Observed properties:
  color: "blue",
  width: 20,
  height: 30,
  // Computed property:
  get area() {
    console.log("Area is calculated");
    return this.width * this.height;
  },
});
// Reaction: prints "Area is calculated"
// and "Rectangle area is 600"
createReaction(
  () => rectangle.area,
  (area) => console.log(`Rectangle area is ${area}`)
);
// Change to an observed property will propagate
// first to computed property, then to the reaction,
// thereby printing "Area is calculated"
// and "Rectangle area is 510":
rectangle.width = 17;
// Note: even though "color" is an observed property,
// no reaction or computed property depends on it. Therefore,
// nothing is printed other than "Rectangle color is red"
rectangle.color = "red";
console.log("Rectangle color is", rectangle.color);
```

#### Computed properties

Computed properties are specified as getters on reactive objects. They are derived from other observed or computed properties. Computed properties of one reactive object may depend on observed / computed properties of other reactive objects. Dependencies are automatically tracked and are dynamic - hence, they can vary over time. Fluid State is designed to only perform recalculations when necessary.

```js
import { createReactive, createReaction } from "fluidstate";
const mother = createReactive({
  income: 55_000,
  get tax() {
    return this.income * 0.2;
  },
});
const father = createReactive({
  income: 34_000,
  get tax() {
    return this.income * 0.1;
  },
});
const son = createReactive({
  income: 15_000,
  get tax() {
    return this.income * 0.05;
  },
});
const family = createReactive({
  isTaxable: true,
  get income() {
    console.log("Calculating total income");
    return mother.income + father.income + son.income;
  },
  get tax() {
    console.log("Calculating total tax");
    return this.isTaxable ? mother.tax + father.tax + son.tax : 0;
  },
});
// Prints "Calculating total tax" and "Total family tax: 15150"
createReaction(
  () => family.tax,
  (tax) => console.log("Total family tax:", tax)
);
// Changing any family member's "income" will re-trigger the reaction
mother.income = 65_000;
// Prints "Calculating total tax" and "Total family tax: 17150" and
// "Mother's new tax is 13000"
console.log("Mother's new tax is", mother.tax);
// However, if we change "isTaxable" flag, the total family tax will
// no longer depend on any family member's properties
family.isTaxable = false;
// Prints "Calculating total tax", "Total family tax: 0" and
// "Is taxable: false"
console.log("Is taxable:", family.isTaxable);
// Now, changing "income" of a family member will print nothing:
mother.income = 65_000;
```

Note: by looking at the above example, you may start wondering, at what exact point do reactions and computed properties get recalculated? This is described in the [Update scheduling section](#update-scheduling).

#### Actions

Actions are functions on reactive objects that may update any number of observed properties. The only difference between directly changing observed properties and calling actions that change observed properties is that actions automatically batch all reactive object mutations. Reactions will not be triggered during an action: they may only be scheduled for after the action is finished. There is some nuance to this, because Fluid State's default scheduling strategy actually also results in batching, so there's a bit of trickery involved in the following example; however, it'll demonstrate the basic point. A more thorough explanation is provided in the [Update scheduling section](#update-scheduling).

```js
import { createReactive, createReaction } from "fluidstate";
const menu = createReactive({
  burgerUsd: 10,
  saladUsd: 5,
  friesUsd: 2,
  drinkUsd: 1,
});
const alex = createReactive({
  paidUsd: 0,
});
const maria = createReactive({
  paidUsd: 0,
});
// Prints "Alex paid: $0"
createReaction(
  () => alex.paidUsd,
  (paidUsd) => console.log(`Alex paid: \$${paidUsd}`)
);
// Prints "Maria paid: $0"
createReaction(
  () => maria.paidUsd,
  (paidUsd) => console.log(`Maria paid: \$${paidUsd}`)
);
const lunch = createReactive({
  get lunchTotal() {
    return alex.paidUsd + maria.paidUsd;
  },
  payForLunch() {
    console.log("Action mutation");
    alex.paidUsd = menu.burgerUsd + menu.friesUsd + menu.drinkUsd;
    maria.paidUsd = menu.saladUsd + menu.drinkUsd;
    console.log("Action mutation finished");
  },
});
// When changing the value without an action, reactions will be triggered
// one after the other:
console.log("Direct mutation");
alex.paidUsd = menu.burgerUsd + menu.friesUsd;
// Prints "Alex paid: $12"
maria.paidUsd = menu.saladUsd;
// Prints "Maria paid: $5"
console.log(`Lunch total: \$${lunch.lunchTotal}`);
console.log("Direct mutation finished");
// However, with an action, reactions will only be triggered at the end.
// Therefore, it prints "Action mutation", "Action mutation finished"
lunch.payForLunch();
// Will trigger the reactions and print "Maria paid: $6" and "Alex paid: $13"
console.log(`Lunch total: \$${lunch.lunchTotal}`);
```

It is strongly recommended (though not required) to use actions to mutate reactive objects, since it is going to be more performant in many cases.

### Reactive objects are proxies

Under the hood, reactive objects are [JS proxy-based](https://github.com/getify/You-Dont-Know-JS/blob/59d33b0c47c214270b87e7afd5670ad864d8a465/es6%20%26%20beyond/ch7.md#proxies) wrappers around original objects that add access and mutation tracking to organize reactivity. Changes to reactive objects propagate to the original inert objects.

```js
import { createReactive } from "fluidstate";
const inertUser = {
  user: "princess1981",
  isAdmin: false,
};
const reactiveUser = createReactive(inertUser);
reactiveUser.isAdmin = true;
// Prints: "inertUser.isAdmin = true after setting reactive"
console.log(`inertUser.isAdmin = ${inertUser.isAdmin} after setting reactive`);
```

### Reactive utilities

#### getInert

`getInert` returns the original inert, non-reactive counterpart of a reactive object. In all normal Fluid State operations, inert objects only contain other inert objects, while reactive objects only contain other reactive objects. Inert objects would not contain reactive objects. `getInert` may be especially useful (1) for debugging and inspecting objects, (2) for handing off reactive objects to other systems that expect regular JS objects.

```js
import { createReactive, createReaction, getInert } from "fluidstate";
const inertUser = {
  user: "princess1981",
  isAdmin: false,
  avatar: {
    src: "https://fluidstate.gitlab.io/fluidstate-100.png",
    size: [100, 100],
  },
};
const reactiveUser = createReactive(inertUser);
// Prints: "reactiveUser.avatar.size = [100,100]"
createReaction(
  () => reactiveUser,
  (user) => console.log("reactiveUser.avatar.size =", user.avatar.size)
);
// Prints: "reactiveUser.avatar.size = [200,100]"
reactiveUser.avatar.size = [200, 100];
setTimeout(() => {
  // Prints: "inertUser.avatar.size = [200,100]"
  console.log("inertUser.avatar.size =", inertUser.avatar.size);
  // Reactive objects are deep proxies over the original objects,
  // which are not equal to the inert objects themselves
  console.log(
    reactiveUser.avatar.size !== inertUser.avatar.size
      ? "Reactive objects are not strictly equal to their inert counterparts"
      : ""
  );
  // If we don't have access to original inert objects, we can still get them
  // by using `getInert`
  console.log(
    getInert(reactiveUser).avatar.size === inertUser.avatar.size
      ? "getInert(reactiveUser).avatar.size === inertUser.avatar.size"
      : ""
  );
  // It does not matter what `getInert` is called on
  console.log(
    getInert(reactiveUser.avatar).size === inertUser.avatar.size
      ? "getInert(reactiveUser.avatar).size === inertUser.avatar.size"
      : ""
  );
  console.log(
    getInert(reactiveUser.avatar.size) === inertUser.avatar.size
      ? "getInert(reactiveUser.avatar.size) === inertUser.avatar.size"
      : ""
  );
});
```

#### getReactive

`getReactive` is the inverse of `getInert`. It returns an existing reactive object for a given inert object. If the reactive object is not yet created around the inert object, `null` is returned.

```js
import { createReactive, createReaction, getReactive } from "fluidstate";
const inertObject = {};
// Prints: "Currently there's no reactive"
console.log(
  getReactive(inertObject) === null ? "Currently there's no reactive" : ""
);
const reactiveObject = createReactive(inertObject);
// Prints: "Now getting reactive works"
console.log(
  getReactive(inertObject) === reactiveObject
    ? "Now getting reactive works"
    : ""
);
```

#### getComputedKeys

`getComputedKeys` returns a JS set containing all computed keys of a reactive object. It may be especially useful for cloning reactive objects and serializing them.

```js
import { createReactive, createReaction, getComputedKeys } from "fluidstate";
const rectangle = createReactive({
  width: 12,
  height: 13,
  get area() {
    return this.width * this.height;
  },
  get diagonal() {
    return Math.sqrt(this.width ** 2 + this.height ** 2);
  },
});
// Prints: 'Computed properties: ["area","diagonal"]'
console.log("Computed properties:", [...getComputedKeys(rectangle)]);
```

## Reactions

Reactions are used to subscribe to parts of reactive objects and perform effects as a result of these changes.

Each reaction consists of two parts:

1. **Reactive part**: this part subscribes the reaction to any reactive properties referenced inside it and any reactive objects returned from it
2. **Inert effect part**: this part runs after the reactive part, does not track property access and does not subscribe the reaction to anything. It receives value returned from the reactive part and can safely react to it. Usually this is the part that performs side-effects such as network requests, re-rendering, etc

Reactions run immediately upon creation.

```js
import { createReactive, createReaction } from "fluidstate";
const person = createReactive({
  firstName: "John",
  lastName: "Smith",
});
// This is a reaction. Upon creation, it runs and prints: "John Smith"
createReaction(
  // Reactive part: subscribes the reaction to `person.firstName` and `person.lastName`
  () => `${person.firstName} ${person.lastName}`,
  // Inert part: can safely react to changes
  (name) => console.log(name)
);
// Since the reaction is subscribed to `person.firstName` and `person.lastName`,
// changes to either will re-trigger it. Hence, the following prints: "Kevin Smith"
person.firstName = "Kevin";
```

If a reactive property is only referenced in inert part, the reaction will **not** re-trigger when such reactive property changes.

```js
import { createReactive, createReaction } from "fluidstate";
const person = createReactive({
  firstName: "John",
  lastName: "Smith",
});
// This is a reaction. Upon creation, it runs and prints: "John Smith"
createReaction(
  // Reactive part: subscribes only to `person.lastName`
  () => person.lastName,
  // Inert part: does not subscribe to reactive properties referenced inside
  (lastName) => console.log(`${person.firstName} ${lastName}`)
);
// Since `person.firstName` is only referenced in inert part, the reaction will not rerun when it changes;
// therefore, nothing else is printed
person.firstName = "Kevin";
```

If inert part's returned value contains a reactive object, the reaction subscribes to the entire reactive object.

```js
import { createReactive, createReaction } from "fluidstate";
const person = createReactive({
  firstName: "John",
  lastName: "Smith",
});
// This is a reaction. Upon creation, it runs and prints: "John Smith"
createReaction(
  // Reactive part: subscribes to the entire "person"
  () => person,
  // Inert part
  ({ firstName, lastName }) => console.log(`${firstName} ${lastName}`)
);
// Any change to person will trigger the reaction. Prints: "Kevin Smith"
person.firstName = "Kevin";
// Later:
setTimeout(() => {
  // Prints: "Kevin Doe"
  person.lastName = "Doe";
});
```

### Reaction cleanup

`createReaction` returns a cleanup function that may be called to stop the reaction.

```js
import { createReactive, createReaction } from "fluidstate";
const reactive = createReactive({ a: 100 });
// Prints: "reactive.a = 100"
const stopLogging = createReaction(
  () => reactive.a,
  (a) => console.log("reactive.a =", a)
);
stopLogging();
reactive.a = 200;
// Note: nothing is printed because the reaction is cleaned up
```

It is crucial to stop reactions whenever they are no longer needed. By default, Fluid State reactions will continue to exist in memory, unless they are manually stopped or the data they depend on is garbage collected. It is best to stop reactions individually like mentioned above, but Fluid State also provides a function `stopAllReactions` to stop all reactions that were created but not stopped yet.

```js
import { createReactive, createReaction, stopAllReactions } from "fluidstate";
const state = createReactive({
  a: 100,
  b: 200,
});
createReaction(
  () => state.a,
  (a) => console.log("a =", a)
);
createReaction(
  () => state.b,
  (b) => console.log("b =", b)
);
let intervalCount = 0;
let interval = setInterval(() => {
  console.log(`Increasing "a" and "b"`);
  state.a++;
  state.b++;
  setTimeout(() => {
    intervalCount++;
    if (intervalCount === 3) {
      console.log("Stopping all reactions");
      stopAllReactions();
    }
    if (intervalCount === 6) {
      clearInterval(interval);
    }
  });
}, 1000);
```

## Update scheduling

By default, when observed properties are mutated, a recalculation of computed properties and reactions is scheduled to be performed at a later point in time. Scheduling behavior is customizable and is designed to automatically batch mutations while also eliminating possibility of reading out-of-date values.

The default scheduling behavior is to perform recalculation of computed properties and reactions at the next JavaScript event loop iteration (i.e. using `setTimeout`). This allows multiple observed properties to be mutated at once without triggering reactions multiple times.

```js
import { createReactive, createReaction } from "fluidstate";
const professor = createReactive({
  firstName: "David",
  lastName: "Brailsford",
  age: 70,
});
// Prints: "Professor David Brailsford is 70 years old"
createReaction(
  () => professor,
  ({ firstName, lastName, age }) =>
    console.log(`Professor ${firstName} ${lastName} is ${age} years old`)
);
// Updating properties:
professor.firstName = "Maryam"; // This schedules a reaction, but does not trigger it yet
professor.lastName = "Mirzakhani"; // The reaction is already scheduled
professor.age = 40; // The reaction is already scheduled
// The reaction is scheduled to run at the end of event loop cycle. So, after
// a `setTimeout`, the reaction reruns and prints:
// "Professor Maryam Mirzakhani is 40 years old"
```

This automatic batching aids efficiency, but would've been problematic without another aspect of scheduling. What if you read a computed property after changing an observed property but before the recalculation is triggered? Or, what if a reaction is supposed to modify another observed property, but you read it before the reaction is triggered? Both situations would result in reading out-of-date values, but Fluid State alleviates this problem by triggering the recalculations and reactions upon value reads. Here's the altered version of the above example that demonstrates this:

```js
import { createReactive, createReaction } from "fluidstate";
const professor = createReactive({
  firstName: "David",
  lastName: "Brailsford",
  age: 70,
});
// Prints: "Professor David Brailsford is 70 years old"
createReaction(
  () => professor,
  ({ firstName, lastName, age }) =>
    console.log(`Professor ${firstName} ${lastName} is ${age} years old`)
);
// Updating properties:
professor.firstName = "Maryam"; // This schedules a reaction, but does not trigger it yet
professor.lastName = "Mirzakhani"; // The reaction is already scheduled
// Next, a reactive value is read, so the update is immediately triggered
// and prints "Professor Maryam Mirzakhani is 70 years old"
console.log("Reading age:", professor.age);
professor.age = 40; // This schedules a reaction again
// The reaction is scheduled to run at the end of event loop cycle. So, after
// a `setTimeout`, the reaction reruns and prints:
// "Professor Maryam Mirzakhani is 40 years old"
```

### Update utilities

#### batch

It may be costly to trigger reactions whenever a read is performed, especially when it is known that read values are not going to go out-of-date. For example,

```js
import { createReactive, createReaction } from "fluidstate";
const values = createReactive([0, 1, 2, 3, 4, 5, 6]);
// Prints the sum of values:
createReaction(
  () => values,
  (values) =>
    console.log(`Sum of values: ${values.reduce((acc, val) => acc + val, 0)}`)
);
// The following code will trigger a reaction at every loop iteration, because it mixes reads and writes:
for (let i = 0; i < values.length; i++) {
  //                       ^
  //                       Read is performed here, so updates are triggered
  values[i] += 1;
  //        ^
  //        Observed property is mutated here, so update is scheduled
}
```

If you'd like to prevent this behavior where reads trigger an update, you should use `batch` function to defer reactions and recalculations to after the batch function finishes running.

```js
import { createReactive, createReaction, batch } from "fluidstate";
const values = createReactive([0, 1, 2, 3, 4, 5, 6]);
// Prints the sum of values:
createReaction(
  () => values,
  (values) =>
    console.log(`Sum of values: ${values.reduce((acc, val) => acc + val, 0)}`)
);
// The following code will trigger the reaction only once at the end of the event loop:
batch(() => {
  for (let i = 0; i < values.length; i++) {
    values[i] += 1;
  }
});
```

As described in [Actions section](#actions), reactive actions (i.e. functions of reactive objects) use this batching, therefore if you only perform mutations in synchronous reactive actions as a rule, you do not need to worry about inefficient updates. However, there may be situations where it is still a bit more convenient to use `batch` - for example, in case of async network requests:

```js
import { createReactive, createReaction, batch } from "fluidstate";
const NOT_LOADED = 0;
const LOADING = 1;
const LOADED = 2;
const responses = createReactive([
  {
    url: "/",
    state: NOT_LOADED,
  },
]);
const stopLoadResponses = createReaction(
  () => responses.filter(({ state }) => state === NOT_LOADED),
  async (responses) => {
    batch(() => {
      responses.forEach((response) => {
        response.state = LOADING;
      });
    });
    responses.forEach(async (response) => {
      const result = await fetch(response.url);
      let text;
      try {
        text = await result.text();
      } catch (_) {
        text = "";
      }
      batch(() => {
        response.content = text;
        response.statusCode = result.status;
        response.state = LOADED;
      });
    });
  }
);
const stopLogResponses = createReaction(
  () => responses,
  (responses) => {
    console.log("---");
    responses.forEach((response) =>
      console.log(
        `Url ${response.url} is ${
          response.state === NOT_LOADED
            ? "not loaded"
            : response.state === LOADING
            ? "loading"
            : "loaded"
        }` +
          (response.statusCode
            ? ` - responded with ${response.statusCode}`
            : ``) +
          (response.content ? ` - had content "${response.content}"` : ``)
      )
    );
  }
);
// Requesting a couple more pages:
responses.push(
  {
    url: "/1",
    state: NOT_LOADED,
  },
  {
    url: "/2",
    state: NOT_LOADED,
  }
);
```

#### triggerUpdate

If you'd like to trigger the recalculations and reactions immediately, without waiting for the scheduled update and without reading a value, you can use `triggerUpdate` function.

```js
import { createReactive, createReaction, triggerUpdate } from "fluidstate";
const professor = createReactive({
  firstName: "David",
  lastName: "Brailsford",
  age: 70,
});
// Prints: "Professor David Brailsford is 70 years old"
createReaction(
  () => professor,
  ({ firstName, lastName, age }) =>
    console.log(`Professor ${firstName} ${lastName} is ${age} years old`)
);
// Updating properties:
professor.firstName = "Maryam"; // This schedules a reaction, but does not trigger it yet
professor.lastName = "Mirzakhani"; // The reaction is already scheduled
triggerUpdate(); // Recalculations and reactions are triggered immediately
professor.age = 40; // This schedules a reaction again
// The reaction is scheduled to run at the end of event loop cycle. So, after
// a `setTimeout`, the reaction reruns and prints:
// "Professor Maryam Mirzakhani is 40 years old"
```

### Customizing update scheduling

As mentioned before, by default, when observed properties are mutated, the recalculation of computed properties and reactions is scheduled to be performed at the end of JavaScript event loop. This behavior is completely customizable by providing custom `schedule` and `cancel` functions as part of Fluid State's scheduler configuration. `schedule` takes the update function, can schedule it or call it immediately, and return any kind of identifier. This identifier is passed to `cancel`, which allows customizing cancelling the scheduled update.

**Example 1**: if you'd like updates to be triggered immediately instead of being scheduled

```js
import {
  createReactive,
  createReaction,
  customizeSchedulerConfig,
} from "fluidstate";
// This configures Fluid State to trigger update immediately
// instead of scheduling it
customizeSchedulerConfig({
  schedule: (update) => {
    update();
  },
  cancel: () => {},
});
const professor = createReactive({
  firstName: "David",
  lastName: "Brailsford",
  age: 70,
});
// Prints: "Professor David Brailsford is 70 years old"
createReaction(
  () => professor,
  ({ firstName, lastName, age }) =>
    console.log(`Professor ${firstName} ${lastName} is ${age} years old`)
);
// Prints: "Professor Maryam Brailsford is 70 years old"
professor.firstName = "Maryam";
// Prints: "Professor Maryam Mirzakhani is 70 years old"
professor.lastName = "Mirzakhani";
// Prints: "Professor Maryam Mirzakhani is 40 years old"
professor.age = 40;
```

**Example 2**: you can also decide to schedule the reactions to be performed at an even later point in time than default, e.g. during the next animation frame via `requestAnimationFrame` or during browser's idle periods via `requestIdleCallback`.

```js
import {
  createReactive,
  createReaction,
  customizeSchedulerConfig,
} from "fluidstate";
// This configures Fluid State to schedule reactions during the idle period
customizeSchedulerConfig({
  schedule: (update) => requestIdleCallback(update),
  cancel: (requestId) => cancelIdleCallback(requestId),
});
const professor = createReactive({
  firstName: "David",
  lastName: "Brailsford",
  age: 70,
});
// Prints: "Professor David Brailsford is 70 years old"
const stopReaction = createReaction(
  () => professor,
  ({ firstName, lastName, age }) =>
    console.log(`Professor ${firstName} ${lastName} is ${age} years old`)
);
professor.firstName = "Maryam";
professor.lastName = "Mirzakhani";
professor.age = 40;
setTimeout(() => {
  // Note: because the reaction is scheduled for the idle callback, it'll be performed after `setTimeout`
  professor.age = 45;
});
// Eventually prints: "Professor Maryam Mirzakhani is 45 years old"
```

## More about Fluid State

Other pages of this documentation explore why Fluid State is designed the way it is, describe primitives that Fluid State is based on, as well as outline suggestions for building reactive apps. Additionally, there are examples, explanation of inspiration, credits and license definition. Feel free to explore other documentation pages to get familiar with Fluid State in detail.

[^1]: [You Don't Know JS](https://github.com/getify/You-Dont-Know-JS/blob/7d3a4b7351515fa41c8e3127376dda1ca98514d8/objects-classes/ch1.md#chapter-1-object-foundations) - one of the most prominent JS books
