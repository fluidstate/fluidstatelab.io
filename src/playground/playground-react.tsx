import * as fluidstate from "fluidstate";
import * as React from "react";
import * as fluidstateReact from "fluidstate-react";
import { createRoot } from "react-dom/client";
import { createCommonNamespace, runCode, win } from "./playground-common";
import { preventInfiniteLoops } from "./playground-infinite-loop-safeguard";
import { replaceTimeouts } from "./playground-timeouts";

const createNamespace = () => {
	return {
		React,
		define: (dependencies: string[], fn: (...args: unknown[]) => unknown) => {
			const exports = {};
			const args = dependencies.map((dependency) => {
				if (dependency === "exports") {
					return exports;
				}
				if (dependency === "react") {
					return React;
				}
				if (dependency === "fluidstate") {
					return fluidstate;
				}
				if (dependency === "fluidstate-react") {
					return fluidstateReact;
				}
				throw new Error(`Unknown dependency ${dependency}`);
			});
			fn(...args);
			return exports;
		},
		...createCommonNamespace(),
	};
};

window.addEventListener("DOMContentLoaded", () => {
	let root = createRoot(document.getElementsByClassName("root")[0]);
	win.runCode = (babel, code) => {
		root.unmount();
		babel.availablePlugins["prevent-infinite-loops"] = preventInfiniteLoops;
		const { code: transformedCode } = babel.transform(code, {
			plugins: ["prevent-infinite-loops", "transform-modules-amd"],
			presets: ["react"],
		});
		replaceTimeouts();
		const { App } = runCode(createNamespace(), transformedCode ?? code);
		if (!App) {
			throw new Error('Please export "App" component from the playground');
		}
		root.render(React.createElement(App));
	};
	win.isLoaded = true;
});
