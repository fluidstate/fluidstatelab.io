import * as fluidstate from "fluidstate";
import * as preact from "preact";
import * as hooks from "preact/hooks";
import * as fluidstatePreact from "fluidstate-preact";
import { createCommonNamespace, runCode, win } from "./playground-common";
import { preventInfiniteLoops } from "./playground-infinite-loop-safeguard";
import { replaceTimeouts } from "./playground-timeouts";

const createNamespace = () => {
	return {
		preact: { ...hooks, h: preact.h, Fragment: preact.Fragment },
		define: (dependencies: string[], fn: (...args: unknown[]) => unknown) => {
			const exports = {};
			const args = dependencies.map((dependency) => {
				if (dependency === "exports") {
					return exports;
				}
				if (dependency === "preact") {
					return preact;
				}
				if (dependency === "preact/hooks") {
					return hooks;
				}
				if (dependency === "fluidstate") {
					return fluidstate;
				}
				if (dependency === "fluidstate-preact") {
					return fluidstatePreact;
				}
				throw new Error(`Unknown dependency ${dependency}`);
			});
			fn(...args);
			return exports;
		},
		...createCommonNamespace(),
	};
};

window.addEventListener("DOMContentLoaded", () => {
	win.runCode = (babel, code) => {
		babel.availablePlugins["prevent-infinite-loops"] = preventInfiniteLoops;
		const { code: transformedCode } = babel.transform(code, {
			plugins: ["prevent-infinite-loops", "transform-modules-amd"],
			presets: [
				[
					"react",
					{
						pragma: "preact.h",
						pragmaFrag: "preact.Fragment",
					},
				],
			],
		});
		replaceTimeouts();
		const { App } = runCode(createNamespace(), transformedCode ?? code);
		if (!App) {
			throw new Error('Please export "App" component from the playground');
		}
		preact.render(<App />, document.getElementsByClassName("root")[0]);
	};
	win.isLoaded = true;
});
