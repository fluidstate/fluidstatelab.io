import fetchMock from "fetch-mock";

const getFetchDelay = () => Math.random() * 300 + 100;

export const getFetch = () =>
	fetchMock
		.sandbox()
		.mock("/", 200, {
			get delay() {
				return getFetchDelay();
			},
			response: {
				body: "Hello world",
			},
		})
		.mock("/1", 200, {
			get delay() {
				return getFetchDelay();
			},
			response: {
				body: "Lorem ipsum",
			},
		})
		.mock("*", 404);
