const win = {
	setTimeout: window.setTimeout as Window["setTimeout"],
	clearTimeout: window.clearTimeout as Window["clearTimeout"],
	setInterval: window.setInterval as Window["setInterval"],
	clearInterval: window.clearInterval as Window["clearInterval"],
	requestAnimationFrame:
		window.requestAnimationFrame as Window["requestAnimationFrame"],
	cancelAnimationFrame:
		window.cancelAnimationFrame as Window["cancelAnimationFrame"],
	requestIdleCallback:
		window.requestIdleCallback as Window["requestIdleCallback"],
	cancelIdleCallback: window.cancelIdleCallback as Window["cancelIdleCallback"],
};

let timeouts: Array<ReturnType<Window["setTimeout"]>> = [];
let intervals: Array<ReturnType<Window["setInterval"]>> = [];
let animationFrames: Array<ReturnType<Window["requestAnimationFrame"]>> = [];
let idleCallbacks: Array<ReturnType<Window["requestIdleCallback"]>> = [];

const clearExistingTimeouts = () => {
	timeouts.forEach((timeout) => win.clearTimeout.call(window, timeout));
	intervals.forEach((interval) => win.clearInterval.call(window, interval));
	animationFrames.forEach((animationFrame) =>
		win.cancelAnimationFrame.call(window, animationFrame)
	);
	idleCallbacks.forEach((idleCallback) =>
		win.cancelIdleCallback.call(window, idleCallback)
	);
	timeouts = [];
	intervals = [];
	animationFrames = [];
	idleCallbacks = [];
};

export const replaceTimeouts = () => {
	clearExistingTimeouts();

	// @ts-ignore
	window.setTimeout = function (
		...args: Parameters<Window["setTimeout"]>
	): ReturnType<Window["setTimeout"]> {
		const timeout = win.setTimeout.apply(this, args);
		timeouts.push(timeout);
		return timeout;
	};

	// @ts-ignore
	window.setInterval = function (
		...args: Parameters<Window["setInterval"]>
	): ReturnType<Window["setInterval"]> {
		const interval = win.setInterval.apply(this, args);
		intervals.push(interval);
		return interval;
	};

	// @ts-ignore
	window.requestAnimationFrame = function (
		...args: Parameters<Window["requestAnimationFrame"]>
	): ReturnType<Window["requestAnimationFrame"]> {
		const animationFrame = win.requestAnimationFrame.apply(this, args);
		animationFrames.push(animationFrame);
		return animationFrame;
	};

	// @ts-ignore
	window.requestIdleCallback = function (
		...args: Parameters<Window["requestIdleCallback"]>
	): ReturnType<Window["requestIdleCallback"]> {
		const animationFrame = win.requestIdleCallback.apply(this, args);
		animationFrames.push(animationFrame);
		return animationFrame;
	};
};
