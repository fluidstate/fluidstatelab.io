import { resetSchedulerConfig } from "fluidstate";
import type Babel from "@babel/standalone";
import { getFetch } from "./playground-fetch-mock";

interface Win extends Window {
	isLoaded: boolean;
	runCode: (babel: typeof Babel, code: string) => void;
}

export const win = window as unknown as Win;

const logger = document.getElementsByClassName("logger")[0];
const root = document.getElementsByClassName("root")[0];

export const createCommonNamespace = () => ({
	console: {
		log: (...args: string[]) => {
			const nodes = args.map((arg) => {
				if (typeof arg === "object") {
					return document.createTextNode(JSON.stringify(arg));
				}
				return document.createTextNode(arg);
			});
			nodes.forEach((node, i) => {
				if (i !== 0) {
					logger.appendChild(document.createTextNode(" "));
				}
				logger.appendChild(node);
			});
			logger.appendChild(document.createTextNode("\n"));
		},
	},
	undefined: undefined,
	Error,
	Number,
	Boolean,
	String,
	Object,
	Array,
	Math,
	JSON,
	RangeError,
	setTimeout,
	setInterval,
	clearTimeout,
	clearInterval,
	Set,
	Map,
	fetch: getFetch(),
	requestAnimationFrame,
	cancelAnimationFrame,
	requestIdleCallback,
	cancelIdleCallback,
});

export const runCode = <T extends object>(namespace: T, code: string) => {
	if (code.indexOf("console.log") >= 0) {
		document.body.classList.add("with-console");
	} else {
		document.body.classList.remove("with-console");
	}
	logger.textContent = "";
	root.textContent = "";
	resetSchedulerConfig();
	const fnCode = `
		with (sandbox.proxy) {
			${Object.keys(namespace)
				.map((name) => `const ${name} = namespace.${name};`)
				.join("\n")}
			sandbox.revoke();
			return ${code}
		}
	`;
	const fn = new Function("sandbox", "namespace", fnCode);
	const sandbox = Proxy.revocable(Object.create(null), {});
	return fn(sandbox, namespace);
};
