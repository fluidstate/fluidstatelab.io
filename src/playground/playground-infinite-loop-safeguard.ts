import {
	type types as babelTypes,
	type template as babelTemplate,
	type Visitor,
	type NodePath,
} from "@babel/core";

type PluginState = {
	traversedPaths?: Set<NodePath<babelTypes.Node>>;
	isFinished?: boolean;
};

/**
 * This is a safeguard for preventing potential infinite loops by inserting
 * a guard for checking that the number of loop iterations doesn't exceed a max.
 * Note: this is taken from https://stackoverflow.com/a/73393992/7842231 by Davor Hrg
 */
export const preventInfiniteLoops = ({
	types: t,
	template,
}: {
	types: typeof babelTypes;
	template: typeof babelTemplate;
}): { visitor: Visitor } => {
	const MAX_SOURCE_ITERATIONS = 10_000;

	const buildGuard = template(`
    if (COUNTER++ > MAX_ITERATIONS) {
      throw new RangeError(
        'Potential infinite loop: exceeded ' +
        MAX_ITERATIONS +
        ' iterations.'
      );
    }
  `) as (
		...args: Parameters<ReturnType<typeof template>>
	) => babelTypes.Statement;

	const loopVisitor = (
		path:
			| NodePath<babelTypes.WhileStatement>
			| NodePath<babelTypes.ForStatement>
			| NodePath<babelTypes.DoWhileStatement>,
		babelState: unknown
	) => {
		const state = babelState as PluginState;

		if (state.isFinished) {
			return;
		}

		if (!state.traversedPaths) {
			state.traversedPaths = new Set();
		}

		if (state.traversedPaths.has(path)) {
			return;
		}

		state.traversedPaths.add(path);

		// A counter that is incremented with each iteration
		const counter = path.scope.parent.generateUidIdentifier("loopSafeguard");
		const counterInit = t.numericLiteral(0);
		path.scope.parent.push({
			id: counter,
			init: counterInit,
		});

		// If statement and throw error if it matches our criteria
		const guard = buildGuard({
			COUNTER: counter,
			MAX_ITERATIONS: t.numericLiteral(MAX_SOURCE_ITERATIONS),
		});

		const bodyPath = path.get("body") as NodePath<babelTypes.Statement>;

		// No block statment e.g. `while (1) 1;`
		if (!bodyPath.isBlockStatement()) {
			bodyPath.replaceWith(t.blockStatement([guard, bodyPath.node]));
		} else {
			bodyPath.unshiftContainer("body", guard);
		}
	};

	return {
		visitor: {
			WhileStatement: { enter: loopVisitor },
			ForStatement: { enter: loopVisitor },
			DoWhileStatement: { enter: loopVisitor },
			Program: {
				exit: (_node, babelState) => {
					const state = babelState as PluginState;
					state.isFinished = true;
				},
			},
		},
	};
};
