import * as fluidstate from "fluidstate";
import { createCommonNamespace, runCode, win } from "./playground-common";
import { preventInfiniteLoops } from "./playground-infinite-loop-safeguard";
import { replaceTimeouts } from "./playground-timeouts";

const createNamespace = () => {
	return {
		define: (dependencies: string[], fn: (...args: unknown[]) => unknown) => {
			const exports = {};
			const args = dependencies.map((dependency) => {
				if (dependency === "exports") {
					return exports;
				}
				if (dependency === "fluidstate") {
					return fluidstate;
				}
				throw new Error(`Unknown dependency "${dependency}"`);
			});
			fn(...args);
			return exports;
		},
		...createCommonNamespace(),
	};
};

window.addEventListener("DOMContentLoaded", () => {
	(win.runCode = (babel, code) => {
		babel.availablePlugins["prevent-infinite-loops"] = preventInfiniteLoops;
		const { code: transformedCode } = babel.transform(code, {
			plugins: ["prevent-infinite-loops", "transform-modules-amd"],
		});
		replaceTimeouts();
		runCode(createNamespace(), transformedCode ?? code);
	}),
		(win.isLoaded = true);
});
