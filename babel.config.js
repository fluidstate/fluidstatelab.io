const { createBaseBabelConfig } = require("./babel.config.base");

module.exports = createBaseBabelConfig();
